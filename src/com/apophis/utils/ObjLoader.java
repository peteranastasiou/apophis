package com.apophis.utils;

import com.apophis.gfx.Mesh;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

// Load wavefront *.obj static 3d files
// By Peter Anastasiou
// Only accepts triangles!
// Normals don't work

//NOTE: when taking vertices from Blender using right click and "n": swap x and y and then negate x

public class ObjLoader {

  private static final String WHITESPACE = " \t\n\r";

  public static Mesh loadMesh(String filename) {
    // Pre-buffers (not in opengl indexed format, each buffer has its own index)
    ArrayList<float[]> vertexList = new ArrayList<>();
    ArrayList<float[]> normalList = new ArrayList<>();
    ArrayList<float[]> texCoordList = new ArrayList<>();
    ArrayList<int[]> indexList = new ArrayList<>();
    
    // Load file:
    File file = new File(filename);
    if(!file.canRead()) {
      return null;
    }
    BufferedReader reader;
    try {
      reader = new BufferedReader(new FileReader(file));
    } catch(FileNotFoundException e) {
      e.printStackTrace(System.out);
      return null;
    }
    int current_material = -1;
    String current_token;
    String current_line;
    int line_number = 0;

    // Find pathname
    int pos = filename.length() - 1;
    while(pos >= 0 && filename.charAt(pos) != '/') {
      pos--;
    }
    String pathname = filename.substring(0, pos + 1);

    try {
      while((current_line = reader.readLine()) != null) {
        current_line = current_line.trim();

        //skip comments
        if(current_line.isEmpty() || current_line.charAt(0) == '#') {
          continue;
        }

        StringTokenizer stk = new StringTokenizer(current_line, WHITESPACE);
        if(stk.hasMoreTokens()) {
          current_token = stk.nextToken();
          if(current_token.equals("v")) {
            if(stk.countTokens() != 3) {
              throw new IllegalArgumentException("ObjLoader only supports three coord vertices");
            }
            float[] vertex = new float[3];
            vertex[0] = Float.parseFloat(stk.nextToken());
            vertex[1] = Float.parseFloat(stk.nextToken());
            vertex[2] = Float.parseFloat(stk.nextToken());
            vertexList.add(vertex);

          } else if(current_token.equals("vt")) {
            if(stk.countTokens() != 2) {
              throw new IllegalArgumentException("ObjLoader only supports two coord texCoords");
            }
            float[] texCoord = new float[2];
            texCoord[0] = Float.parseFloat(stk.nextToken());
            texCoord[1] = Float.parseFloat(stk.nextToken());
            texCoordList.add(texCoord);

          } else if(current_token.equals("vn")) {
            if(stk.countTokens() != 3) {
              throw new IllegalArgumentException("ObjLoader only supports three coord normals");
            }
            float[] normal = new float[3];
            normal[0] = Float.parseFloat(stk.nextToken());
            normal[1] = Float.parseFloat(stk.nextToken());
            normal[2] = Float.parseFloat(stk.nextToken());
            normalList.add(normal);

          } else if(current_token.equals("f")) {
            if(stk.countTokens() != 3) {
              throw new IllegalArgumentException("ObjLoader only supports triangle faces");
            }
            int[] indices = new int[9];
            int i = 0;
            // for each vertex of the triangle:
            for(int j = 0; j < 3; j ++) {
              StringTokenizer stk2 = new StringTokenizer(stk.nextToken(),"/");
              if(stk2.countTokens() != 3) {
                 throw new IllegalArgumentException("ObjLoader only supports face indexes in format: v/vt/vn");
              }
              indices[i++] = Integer.parseInt(stk2.nextToken());
              indices[i++] = Integer.parseInt(stk2.nextToken());
              indices[i++] = Integer.parseInt(stk2.nextToken());
            }
            indexList.add(indices);

          } else if(current_token.equals("vp")) {
            // Vertex Parameter - Not Implemented

          } else if(current_token.equals("o")) {
            // Object Name - Not Implemented

          } else if(current_token.equals("usemtl")) {
            // Use Material File - Not Implemented

          } else if(current_token.equals("s")) {
            // Smooth Shading - Not Implemented

          } else if(current_token.equals("g")) {
            // Group - Not Implemented
            
          } else if(current_token.equals("l")) {
            // Line - Not Implemented

          }
        }
      }

    } catch(IOException e) {
      e.printStackTrace(System.err);
    } finally {
      try {
        reader.close();
      } catch(IOException e) {
        e.printStackTrace(System.err);
      }
    }
    
    // Merge indices by choosing longest list and copying data out to match indices:
    // This will allow us to use one indexbuffer for all three attributes.
    int length;
    int choice; // vertex = 0, texCoord = 1, normal = 2
    int vl = vertexList.size();
    int nl = normalList.size();
    int tl = texCoordList.size();
    if(vl > nl && vl > tl) {
      length = vl;
      choice = 0;
    } else if(tl > nl) {
      length = tl;
      choice = 1;
    } else {
      length = tl;
      choice = 1;
      // TODO Normals don't work as the base index!
      //length = nl;
      //choice = 2;
    }
    //println("Choose:"+choice);
    
    int indexCount = indexList.size();
    
    float[] vertices = new float[length*3];
    float[] texCoords = new float[length*2];
    float[] normals = new float[length*3];
    int[] indices = new int[indexCount*3];
    
    for(int i = 0; i < indexList.size(); i ++) {
      for(int j = 0; j < 3; j ++) {
        int[] face = indexList.get(i);
        // old indexes:
        int vi = face[j*3+0]-1;
        int ti = face[j*3+1]-1;
        int ni = face[j*3+2]-1;
        
        // new merged index:
        int index = face[j*3+choice]-1; // 1indexed->0indexed
        
        // save new index:
        indices[i*3+j] = index; // 0indexed->1indexed
        
        // save data:
        float[] vertex = vertexList.get(vi); // 1indexed->0indexed
        vertices[index*3] = vertex[0];
        vertices[index*3+1] = vertex[1];
        vertices[index*3+2] = vertex[2];

        float[] texCoord = texCoordList.get(ti); // 1indexed->0indexed
        texCoords[index*2] = texCoord[0];
        texCoords[index*2+1] = 1-texCoord[1]; // OBJ considers (0, 0) to be the top left of a texture, OpenGL considers it to be the bottom left
        
        float[] normal = normalList.get(ni); // 1indexed->0indexed
        normals[index*3] = normal[0];
        normals[index*3+1] = normal[1];
        normals[index*3+2] = normal[2];
      }
    }
    /*
    for(int i = 0; i < indices.length/3; i ++) {
      print("i"+i+": "+indices[i*3]+", ");
      print(indices[i*3+1]+", ");
      println(""+indices[i*3+2]);
    }
    
    for(int i = 0; i < vertices.length/3; i ++) {
      print("v"+i+": "+vertices[i*3]+", ");
      print(vertices[i*3+1]+", ");
      println(""+vertices[i*3+2]);
    }
    
    for(int i = 0; i < texCoords.length/2; i ++) {
      print("t"+i+": "+texCoords[i*2]+", ");
      println(""+texCoords[i*2+1]);
    }
    */
    Mesh mesh = new Mesh(indices.length);

    mesh.loadVertexBuffer(vertices);
    mesh.loadTexCoordBuffer(texCoords);
    //mesh.loadNormalBuffer(normals);
    mesh.loadIndexBuffer(indices);
    
    return mesh;
  }
}
