package com.apophis.lib;

import static com.apophis.Main.HEIGHT;
import static com.apophis.Main.WIDTH;
import com.apophis.entity.Template;
import com.apophis.gfx.Color;
import com.apophis.gfx.Mesh;
import com.apophis.gfx.graphic.MonoFont;
import com.apophis.gfx.graphic.Polyline;
import com.apophis.gfx.Shader;
import com.apophis.gfx.Texture;
import com.apophis.maths.Vector3d;
import com.apophis.utils.ObjLoader;
import com.apophis.utils.ShaderUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class Lib {

  // Type prefix to keys (internal to this class)
  public static final char POLYLINE = 'P';
  public static final char SHADER = 'S';
  public static final char MESH = 'M';
  public static final char TEXTURE = 'X';
  public static final char TEMPLATE = 'T';
  public static final char FONT = 'F';

  private static final Map<String, Object> store = new LinkedHashMap<>();

  public static boolean loadResources() {
    if(!loadShaders()) return false;
    if(!loadMeshes()) return false;
    if(!loadFonts()) return false;
    if(!loadTextures()) return false;
    if(!loadPolyLines()) return false;
    if(!loadTemplates()) return false;

    Lib.getShader("starnest").setUniform2f("resolution", WIDTH, HEIGHT);

    System.out.println("::::: Library :::::");
    for(String key : store.keySet()) {
      switch(key.charAt(0)) {
        case POLYLINE:
          System.out.print("[P]olyline ");
          break;
        case SHADER:
          System.out.print("[S]hader ");
          break;
        case MESH:
          System.out.print("[M]esh ");
          break;
        case TEXTURE:
          System.out.print("Te[x]ture ");
          break;
        case TEMPLATE:
          System.out.print("[T]emplate ");
          break;
        case FONT:
          System.out.print("[F]ont ");
          break;
      }
      key = key.substring(1);
      System.out.println(key);
    }
    System.out.println(":::::::::::::::::::");
    return true;
  }

  private static boolean loadPolyLines() {
    File folder = new File("res/polylines");
    File[] files = folder.listFiles();
    for(File file : files) {
      int ext = file.getName().lastIndexOf('.');
      String name = file.getName().substring(0, ext);
      Polyline pl = new Polyline(false);
      try {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        boolean nonEmpty = false;
        while((line = reader.readLine()) != null) {
          if(line.trim().isEmpty()) {
            continue;
          }
          String[] parts = line.split(",");
          if(parts.length != 3) {
            System.err.println(name + ": Expected three values: " + line);
            return false;
          }
          Vector3d pt = new Vector3d(
            Double.parseDouble(parts[0]),
            Double.parseDouble(parts[1]),
            Double.parseDouble(parts[2]));
          pl.addPoint(pt);
          nonEmpty = true;
        }
        if(nonEmpty) {
          pl.setColor(Color.WHITE);
          pl.load();
          store.put(POLYLINE + name, pl);
        }
      } catch(IOException | NumberFormatException e) {
        e.printStackTrace(System.err);
      }
    }

    Polyline line = new Polyline(false);
    line.addPoint(new Vector3d(-1, 0, 0));
    line.addPoint(new Vector3d(1, 0, 0));
    line.setColor(Color.YELLOW);
    store.put(POLYLINE + "line", line);

    Polyline circ = Polyline.newCircle(1, 100);
    circ.setColor(Color.WHITE);
    store.put(POLYLINE + "big_circle", circ);

    Polyline smallcirc = Polyline.newCircle(1, 30);
    smallcirc.setColor(Color.WHITE);
    store.put(POLYLINE + "small_circle", smallcirc);

    return true;
  }

  private static boolean loadMeshes() {
    File folder = new File("res/meshes");
    File[] files = folder.listFiles();
    for(File file : files) {
      int ext = file.getName().lastIndexOf('.');
      String name = file.getName().substring(0, ext);
      Mesh mesh = ObjLoader.loadMesh(file.getAbsolutePath());
      store.put(MESH + name, mesh);
    }
    {
      float[] vertices = new float[]{
        1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f
      };

      int[] indices = new int[]{
        0, 1, 2,
        0, 2, 3
      };

      float[] tcs = new float[]{
        1, 0,
        1, 1,
        0, 1,
        0, 0
      };
      Mesh mesh = new Mesh(indices.length);
      mesh.loadVertexBuffer(vertices);
      mesh.loadTexCoordBuffer(tcs);
      mesh.loadIndexBuffer(indices);
      store.put(MESH + "square", mesh);
    }
    
    {
      float[] vertices = new float[]{
        1.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f
      };

      int[] indices = new int[]{
        0, 1, 2,
        0, 2, 3
      };

      float[] tcs = new float[]{
        1, 0,
        1, 1,
        0, 1,
        0, 0
      };
      Mesh mesh = new Mesh(indices.length);
      mesh.loadVertexBuffer(vertices);
      mesh.loadTexCoordBuffer(tcs);
      mesh.loadIndexBuffer(indices);
      store.put(MESH + "rect", mesh);
    }
    {
      // Create rectangle for displaying the text
      float[] vertices = new float[]{
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 0.0f
      };

      float[] tcs = new float[]{
        0, 1,
        0, 0,
        MonoFont.XSHIFT, 0,
        MonoFont.XSHIFT, 1
      };

      int[] indices = new int[]{
        0, 1, 2,
        0, 2, 3
      };

      Mesh mesh = new Mesh(indices.length);
      mesh.loadVertexBuffer(vertices);
      mesh.loadTexCoordBuffer(tcs);
      mesh.loadIndexBuffer(indices);
      store.put(MESH + "font_mesh", mesh);
    }
    return true;
  }

  private static boolean loadFonts() {
    MonoFont fontSmall = new MonoFont("res/fonts/font_04b03.png", 8, 8);
    MonoFont fontBig = new MonoFont("res/fonts/font_speculum.png", 27, 27);
    fontSmall.setAllCaps(true);

    store.put(FONT + "fontSmall", fontSmall);
    store.put(FONT + "fontBig", fontBig);
    return true;
  }

  private static boolean loadTextures() {
    File folder = new File("res/textures");
    File[] files = folder.listFiles();
    for(File file : files) {
      if(!file.getName().contains(".png")) continue;
      int ext = file.getName().lastIndexOf('.');
      String name = file.getName().substring(0, ext);
      //System.out.println("Loading Texture: " + name);
      Texture tex = new Texture(file.getAbsolutePath());
      store.put(TEXTURE + name, tex);
    }
    return true;
  }

  private static boolean loadTemplates() {
    // Scale pos, vel and radius(scale) 100 times smaller than in reality:
    Template sun = new Template("sun", Template.ClassType.PLANET);
    sun.put("style", 4);
    sun.put("pos", Vector3d.ZERO);
    sun.put("mass", 1E29);
    sun.put("scale", 8000.);
    store.put(TEMPLATE + "sun", sun);

    Template earth = new Template("earth", Template.ClassType.PLANET);
    earth.put("style", 0);
    earth.put("pos", new Vector3d(1E6, 0, 0));
    earth.put("mass", 7E23);
    earth.put("scale", 650.);
    store.put(TEMPLATE + "earth", earth);

    Template moon = new Template("moon", Template.ClassType.PLANET);
    moon.put("style", 1);
    moon.put("pos", new Vector3d(6000, 0, 0));
    moon.put("mass", 2E22);
    moon.put("scale", 150.);
    store.put(TEMPLATE + "moon", moon);

    Template mars = new Template("mars", Template.ClassType.PLANET);
    mars.put("style", 2);
    mars.put("pos", new Vector3d(1.3E6, 0, 0));
    mars.put("mass", 5E23);
    mars.put("scale", 600.);
    store.put(TEMPLATE + "mars", mars);

    store.put(TEMPLATE + "scraft", new Template("scraft", Template.ClassType.CRAFT));
    return true;
  }

  private static boolean loadShaders() {
    File folder = new File("res/shaders");
    File[] files = folder.listFiles();
    for(File vertfile : files) {
      if(vertfile.getName().endsWith(".vp")) {
        int ext = vertfile.getName().lastIndexOf('.');
        String shaderName = vertfile.getName().substring(0, ext);
        // search for matching frag:
        File fragfile = new File("res/shaders/" + shaderName + ".fp");
        if(!fragfile.exists()) {
          System.err.println("Couldn't find matching fragment shader file for '" + shaderName + "'");
          return false;
        }
        Shader shader = ShaderUtils.load(shaderName, vertfile.getPath(), fragfile.getPath());
        if(shader == null) {
          System.err.println("Failed to load shader '" + shaderName + "'");
          return false;
        }
        // ALWAYS SET TEX TO ONE!
        if(shader.hasUniform("tex")) {
          shader.setUniform1i("tex", 1);
        }

        store.put(SHADER + shaderName, shader);
        //System.out.println("Loaded Shader '" + shaderName + "' from " + vertfile.getPath() + ", " + fragfile.getPath());
      }
    }
    return true;
  }

  public static Object get(String name) {
    String key = name.substring(1);
    switch(name.charAt(0)) {
      case POLYLINE:
        return getPolyline(key);
      case SHADER:
        return getShader(key);
      case MESH:
        return getMesh(key);
      case TEXTURE:
        return getTexture(key);
      case TEMPLATE:
        return getTemplate(key);
      case FONT:
        return getFont(key);
      default:
        return null;
    }
  }

  public static Shader getShader(String key) {
    Shader shader = (Shader)store.get(SHADER + key);
    if(shader == null) {
      throw new RuntimeException("No such shader: '" + key + "'");
    }
    return shader;
  }

  public static Mesh getMesh(String key) {
    Mesh mesh = (Mesh)store.get(MESH + key);
    if(mesh == null) {
      throw new RuntimeException("No such mesh: '" + key + "'");
    }
    return mesh;
  }

  public static MonoFont getFont(String key) {
    MonoFont mesh = (MonoFont)store.get(FONT + key);
    if(mesh == null) {
      throw new RuntimeException("No such font: '" + key + "'");
    }
    return mesh;
  }

  public static Texture getTexture(String key) {
    Texture tex = (Texture)store.get(TEXTURE + key);
    if(tex == null) {
      throw new RuntimeException("No such texture: '" + key + "'");
    }
    return tex;
  }

  public static Polyline getPolyline(String key) {
    Polyline pl = (Polyline)store.get(POLYLINE + key);
    if(pl == null) {
      throw new RuntimeException("No such polyline: '" + key + "'");
    }
    return pl;
  }

  public static Template getTemplate(String key) {
    Template tmp = (Template)store.get(TEMPLATE + key);
    if(tmp == null) {
      throw new RuntimeException("No such template: '" + key + "'");
    }
    return tmp;
  }
}
