package com.apophis.entity;

import com.apophis.Const;
import com.apophis.Universe;
import com.apophis.lib.Lib;
import com.apophis.controller.*;
import com.apophis.gfx.graphic.Graphic;
import com.apophis.gfx.graphic.PolyTrace;
import com.apophis.maths.Vector3d;
import com.apophis.physics.*;
import com.apophis.utils.Rando;

public final class Factory {

  public static int makePlanet(Universe u, String templateStr, int parent, long time) {
    //System.out.println("Making Planet: "+template.toString());
    Entity e = new Entity(templateStr);
    e.setName(templateStr);
    
    Template template = Lib.getTemplate(templateStr);
    e.setPlanet(new Planet(template)); // save original template not lib ref
    
    int style = (int)template.get("style");
    double mass = (double)template.get("mass");
    double scale = (double)template.get("scale");

    // Relative pos, vel:
    Vector3d pos = (Vector3d)template.get("pos");
    Vector3d vel;
    double radius = pos.length();
    // TODO ASSUMES POS is in the form x,0,0 => vel is 0,y,0
    if(parent == -1) {
      vel = Vector3d.ZERO;
    } else {
      double speed = Math.sqrt(Const.G * u.get(parent).getMass() / radius);
      vel = new Vector3d(0, speed, 0);
    }

    // Make absolute:
    if(parent != -1) {
      pos = pos.add(u.get(parent).getPosAbs());
      vel = vel.add(u.get(parent).getVelAbs());
    }
    e.setMass(mass);
    
    e.setSymbol("small_circle", scale/100);

    // Set Physics:
    e.setPhysics(new OrbitalPhysics(e, pos, vel, false, time));

    // Generate meshes and graphics within Planet class:
    // This is intentional so that Planet can regenerate the entity graphics
    // from the template without having to serialize the meshes!
    e.getPlanet().generate(e);
    
    // add to universe:
    u.addPlanet(e);
    return e.getId();
  }

  public static int makeCraft(Universe u, String templateStr, int parent, long time) {
    return makeCraft(u, templateStr, parent, -1, time);
  }
  
  public static int makeCraft(Universe u, String templateStr, int parent, int seg, long time) {
    //Template template = Lib.getTemplate(templateStr);
    Entity entity = new Entity(templateStr);
    entity.setController(new CraftInputController());
    entity.setMass(10000);
    entity.setViewFollows(true);
    double scale = 4;

    String name = "ship8";
    entity.setName(name);

    Graphic graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setTexture(name);
    graphic.setShader("sprite");
    graphic.setScale(scale);
    entity.addGraphic(graphic);

    graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("plume");
    graphic.setOffset(new Vector3d(-scale * .7, -1.1, 2.0));
    graphic.setLayer(Const.LAYER_FEATURE);
    entity.addGraphic(graphic);

    graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("plume");
    graphic.setOffset(new Vector3d(-scale * .7, 1.1, 2.0));
    graphic.setLayer(Const.LAYER_FEATURE);
    entity.addGraphic(graphic);

    entity.setOutline(name);
    entity.setSymbol("arrow", .05);

    PolyTrace trace = new PolyTrace(u.get(parent).getOutline());
    if(seg >= 0) {
      trace.setPos(seg, Rando.randDouble(0., 1.));
    } else {
      trace.setRandomPos();
    }
    trace.setOffset(2 * scale);
    Vector3d pos = trace.getPos().add(u.get(parent).getPosAbs()); // Position absolute
    entity.setAngle(trace.getTraceAngle() + Math.PI / 2);
    entity.setParent(parent);
    entity.setPhysics(new OrbitalPhysics(entity, pos, u.get(parent).getVelAbs(), true, time));

    // add to universe:
    u.addEntity(entity);
    return entity.getId();
  }

  public static int makeMob(Universe u, String templateStr, int parent, long time) {
    //Template template = Lib.getTemplate(templateStr);
    
    double scale = 1; // 1m
    Graphic graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("sprite");
    graphic.setTexture("alienBlue");
    graphic.setNumFrames(4);
    graphic.setLayer(Const.LAYER_MOB);
    graphic.setScale(scale);

    PolyTrace trace = new PolyTrace(u.get(parent).getOutline());
    trace.setRandomPos(); // Pick random location on surface!
    trace.setOffset(scale);

    Entity entity = new Entity(templateStr);
    entity.setParent(parent);
    entity.addGraphic(graphic);
    entity.setController(new CrawlerAIController());
    entity.setPhysics(new OrbitalPhysics(entity, trace, true, time));
    entity.setName("Crawler");
    entity.setOutline("mech");
    
    // add to universe:
    u.addEntity(entity);
    return entity.getId();
  }

  public static int makeHero(Universe u, String templateStr, int parent, long time) {
    return makeHero(u, templateStr, parent, -1, time);
  }
  
  public static int makeHero(Universe u, String templateStr, int parent, int seg, long time) {
    //Template template = Lib.getTemplate(templateStr);
    double scale = 1; //1m

    Graphic graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("sprite");
    graphic.setTexture("alienGreen");
    graphic.setNumFrames(4);
    graphic.setLayer(Const.LAYER_MOB);
    graphic.setScale(scale);

    PolyTrace trace = new PolyTrace(u.get(parent).getOutline());
    if(seg >= 0) {
      trace.setPos(seg, Rando.randDouble(0., 1.));
    } else {
      trace.setRandomPos(); // Pick random location on surface!
    }
    trace.setOffset(scale);

    Entity entity = new Entity(templateStr);
    entity.setParent(parent);
    entity.addGraphic(graphic);
    entity.setController(new WalkerInputController());
    entity.setPhysics(new OrbitalPhysics(entity, trace, true, time));
    entity.setName("Hero");
    entity.setSymbol("small_circle", .05);
    entity.setOutline("mech");
    
    // add to universe:
    u.addEntity(entity);
    return entity.getId();
  }

  public static int makeProjectile(Universe u, String templateString, int parent, Vector3d pos, long time) {
    double scale = 0.5;

    Graphic graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("sprite");
    graphic.setTexture(templateString);
    graphic.setScale(scale);
    graphic.setLayer(Const.LAYER_PLANET);
    
    Entity entity = new Entity(null);
    entity.setParent(parent);
    entity.setPosRel(pos);
    entity.setAngle(0);
    entity.addGraphic(graphic);
    entity.setName("Projectile");
    
    // TODO Physics
    
    // add to universe:
    u.addEntity(entity);
    return entity.getId();
  }
  
  public static int makeDecoration(Universe u, String templateString, int parent, long time) {
    double scale = Rando.randDouble(1, 5);

    Graphic graphic = new Graphic();
    graphic.setMesh("square");
    graphic.setShader("sprite");
    graphic.setTexture(templateString);
    graphic.setScale(scale);
    graphic.setLayer(Const.LAYER_FEATURE);
    graphic.setFlip(Rando.oneIn(2), false);

    PolyTrace trace = new PolyTrace(u.get(parent).getOutline());
    trace.setRandomPos();
    trace.setOffset(scale * .7);

    Entity entity = new Entity(null);
    entity.setParent(parent);
    entity.setPosRel(trace.getPos());
    entity.setAngle(trace.getAngle());
    entity.addGraphic(graphic);
    entity.setName("Plant");
    
    // add to universe:
    u.addEntity(entity);
    return entity.getId();
  }
}
