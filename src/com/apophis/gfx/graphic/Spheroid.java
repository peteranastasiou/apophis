package com.apophis.gfx.graphic;

import com.apophis.gfx.Color;
import com.apophis.gfx.ColorScheme;
import com.apophis.gfx.Mesh;
import com.apophis.maths.Vector3d;
import com.apophis.utils.SimplexNoise;

public class Spheroid {

  private final Graphic graphic;
  private final Vector3d[] edge; // Edge using furthest point for each longitude line
  private double max_r;

  //TODO apply scale to points?
  public Spheroid(Vector3d pos, ColorScheme scheme, float bumpiness, double smoothness, double scale) {
    // params:
    int size = 256;
    double oct1 = 0.9*smoothness;   // smoothness of first noise octave
    double oct2 = 0.35*smoothness; // smoothness of second noise octave
    double oct3 = 0.15*smoothness; // smoothness of second noise octave

    // Number of segments:
    int vDiv = size / 2;
    int hDiv = size;

    // create arrays:
    float[] vertices = new float[3 * vDiv * hDiv];
    float[] normals = new float[3 * vDiv * hDiv];
    int[] indices = new int[6 * vDiv * hDiv];
    float[] colors = new float[3 * vDiv * hDiv];
    edge = new Vector3d[hDiv];

    float vf = 1.0f / (float)(vDiv - 1);
    float hf = 1.0f / (float)(hDiv - 1); // 1.0f->sphere, 0.5f->hemisphere
    int vi, hi;

    int vertexCount = 0;
    int colorCount = 0;
    int normalCount = 0;
    int indexCount = 0;

    // max and min displacement about radius 1.0 (waterline):
    float r1 = 0.15f;
    float r0 = 0.15f;

    float max = 0.5f + (float)SimplexNoise.noise(0, 0, 0);
    float min = max;

    // Construct vertices of sphere
    //for(vi = 0; vi < vDiv; vi++) { // south to north (out of screen) divisions
    for(vi = vDiv/2 - 1; vi < vDiv; vi++) { // perimeter to north (out of screen) divisions
      for(hi = 0; hi < hDiv; hi++) { // clockwise divisions
        // NORTH POLE IS AT TOP:
        //float x = (float)(Math.cos(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        //float y = (float)(Math.sin(-Math.PI / 2 + Math.PI * vi * vf));
        //float z = (float)(Math.sin(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        // NORTH POLE IS CENTER:
        float x = (float)(Math.cos(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        float y = -(float)(Math.sin(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        float z = (float)(Math.sin(-Math.PI / 2 + Math.PI * vi * vf));
        
        // In this case the normals are the vertices! (Already Normalised)
        normals[normalCount++] = x;
        normals[normalCount++] = y;
        normals[normalCount++] = z;
                
        float no1 = 0.5f + (float)SimplexNoise.noise(pos.x + x / oct1, pos.y + y / oct1, z / oct1);
        float no2 = 0.5f + (float)SimplexNoise.noise(pos.x + x / oct2, pos.y + y / oct2, z / oct2);
        float no3 = 0.5f + (float)SimplexNoise.noise(pos.x + x / oct3, pos.y + y / oct3, z / oct3);
        float noise = 0.9f * no1 + 0.1f * no2 + 0.1f * no3;
        if(noise > max) max = noise;
        if(noise < min) min = noise;

        // Radius Displacement:
        float r = 1.0f + (r1 + r0) * bumpiness * noise - r0;
        
        // Apply Displacement:
        x *= r;
        y *= r;
        z *= r;

        if(edge[hi] == null) {
          edge[hi] = new Vector3d(scale*x, scale*y, 0);
        } else {
          Vector3d edgePt = edge[hi];
          double dist1 = scale*scale*(x * x + y * y);
          double dist2 = edgePt.x * edgePt.x + edgePt.y * edgePt.y;
          // Check whether new maximum for this line of longitude:
          if(dist1 > dist2) {
            edge[hi] = new Vector3d(scale*x, scale*y, 0);
            // Check whether this is the new radial maximum
            if(dist1 > max_r) {
              max_r = dist1;
            }
          }
        }
        // Add vertex data:
        vertices[vertexCount++] = x;
        vertices[vertexCount++] = y;
        vertices[vertexCount++] = z;

        Color c = scheme.getColor(noise);

        colors[colorCount++] = c.r;
        colors[colorCount++] = c.g;
        colors[colorCount++] = c.b;
      }
    }
    
    // Drag Perimeter to the maximum edge to smooth out any edge discrepancies
    for(hi = 0; hi < hDiv; hi++) {
      vertices[3*hi+0] = (float)(edge[hi].x/scale);
      vertices[3*hi+1] = (float)(edge[hi].y/scale);
    }
    //System.out.println("Noise:{" + min + "," + max + "}");
    // Construct indices
    // Start with special case of bottom sub-division where all vertices meet
    /*
    vi = 0;
    for(hi = 0; hi < hDiv - 1; hi++) {
      indices[indexCount++] = vi * hDiv + hi;
      indices[indexCount++] = (vi + 1) * hDiv + hi;
      indices[indexCount++] = (vi + 1) * hDiv + (hi + 1);
    }
    */
    // Middle divisions
    //for(vi = vDiv/2 - 1; vi < vDiv - 2; vi++) { // vi = 1; vi < vDiv - 2; vi++) {
    for(vi = 0; vi < vDiv/2 - 1; vi++) {
      for(hi = 0; hi < hDiv - 1; hi++) {
        indices[indexCount++] = vi * hDiv + hi;
        indices[indexCount++] = (vi + 1) * hDiv + (hi + 1);
        indices[indexCount++] = vi * hDiv + (hi + 1);
        indices[indexCount++] = vi * hDiv + hi;
        indices[indexCount++] = (vi + 1) * hDiv + hi;
        indices[indexCount++] = (vi + 1) * hDiv + (hi + 1);
      }
    }
    
    // Cap off the top of the sphere
    //vi = vDiv - 2;
    vi = vDiv/2 - 1;
    for(hi = 0; hi < hDiv - 1; hi++) {
      indices[indexCount++] = vi * hDiv + hi;
      indices[indexCount++] = (vi + 1) * hDiv + (hi + 1);
      indices[indexCount++] = vi * hDiv + (hi + 1);
    }
    Mesh mesh = new Mesh(indexCount);
    mesh.loadVertexBuffer(vertices);
    mesh.loadColorBuffer(colors);
    mesh.loadNormalBuffer(normals);
    mesh.loadIndexBuffer(indices);
    
    graphic = new Graphic();
    graphic.setMesh(mesh);
    graphic.setShader("color_lit");
  }

  public Polyline getEdgePolyline() {
    Polyline poly = new Polyline(false);
    for(Vector3d v : edge) {
      poly.addPoint(v);
    }
    return poly;
  }
  
  public double getMaxR() {
    return Math.sqrt(max_r);
  }

  public Graphic getGraphic() {
    return graphic;
  }
}
