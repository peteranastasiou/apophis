package com.apophis.lib;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * The Resource class allows for shared data (via the library) or unique data to
 * be held by the same variable type.
 * @param <T> the type of resource
 */
public class Res<T> implements Serializable {

  public String key;
  public transient T contents;

  /**
  * Set the contents to a unique instance
   * @param contents
  */
  public Res(T contents) {
    //new Exception().printStackTrace(System.err);
    this.contents = contents;
  }

  /**
   * Set the resource to point to a shared library
   * @param type
   * @param key 
   */
  public Res(char type, String key) {
    this.key = type + key;
  }

  /**
   * Retrieve the resource either from the library or from contents
   * @return the retrieved object
   */
  public T get() {
    if(key == null) return contents;
    else return (T)Lib.get(key);
  }
}
