package com.apophis.utils;

import java.awt.Point;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

public class Input {

  public static final KeyInput keyIn = new KeyInput();
  public static final CursorInput cursorIn = new CursorInput();
  public static final MouseInput mouseIn = new MouseInput();

  public static class KeyInput extends GLFWKeyCallback {
    private static final int SIZE = 65536;
    public final boolean[] keys = new boolean[SIZE];
    public final boolean[] pressed = new boolean[SIZE];

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
      //System.out.println("key:"+key+" scancode:"+scancode+" a:"+action+" mods:"+mods);
      if(key > 0 && key < 65536) {
        keys[key] = action != GLFW.GLFW_RELEASE;

        if(action == GLFW.GLFW_PRESS) {
          pressed[key] = true;
        }
      }
    }
    
    public void clear() {
      // TODO IMPLEMENT - perhaps linked list instead of array and this sets to null?
    }
  }

  public static class CursorInput extends GLFWCursorPosCallback {

    public double x = 0.0;
    public double y = 0.0;

    @Override
    public void invoke(long window, double xpos, double ypos) {
      x = xpos;
      y = ypos;
    }
  }

  public static class MouseInput extends GLFWMouseButtonCallback {

    public final Point[] down = new Point[4];
    public final Point[] up = new Point[4];

    @Override
    public void invoke(long window, int button, int action, int mods) {
      Point point = new Point((int)cursorIn.x, (int)cursorIn.y);

      if(action == GLFW.GLFW_RELEASE) {
        up[button] = point;
      } else if(action == GLFW.GLFW_PRESS) {
        down[button] = point;
      }
    }
    public void clear() {
      // TODO IMPLEMENT - perhaps linked list instead of array and this sets to null?
    }
  }

  public static float getMouseX() {
    return (float)cursorIn.x;
  }

  public static float getMouseY() {
    return (float)cursorIn.y;
  }

  public static Point wasMouseDown(int btn) {
    Point p = mouseIn.down[btn];
    mouseIn.down[btn] = null;
    return p;
  }

  public static Point wasMouseUp(int btn) {
    Point p = mouseIn.up[btn];
    mouseIn.up[btn] = null;
    return p;
  }

  public static boolean isKeyPressed(int keycode) {
    return keyIn.keys[keycode];
  }

  public static boolean wasKeyPressed(int keycode) {
    boolean b = keyIn.pressed[keycode];
    keyIn.pressed[keycode] = false;
    return b;
  }
  
  /**
   * Clear all stored wasPressed data
   * This prevents a long past key press being picked up
   * Each frame this should be called.
   */
  public static void clear() {
    keyIn.clear();
    mouseIn.clear();
  }
}
