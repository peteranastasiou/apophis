
package com.apophis.gfx.graphic;

import com.apophis.maths.Line3d;
import com.apophis.maths.Vector3d;
import com.apophis.maths.Vector3f;
import com.apophis.utils.Rando;
import java.io.Serializable;
import java.util.List;

public final class PolyTrace implements Serializable {
  private final Polyline polyline;
  private final int maxId;
  
  private Line3d lineSegment = null;
  private int segId;
  private double ratio;
  private double offset_r, offset_a; // polar coordinates of position offset
  private double angleOffset; // angle offset
  private double length;
  
  public PolyTrace(Polyline polyline) {
    this.polyline = polyline;
    maxId = polyline.getPoints().size() - 2; // last point is same as first
    if(segId > maxId) segId = 0;
  }
  
  public void setRandomPos() {
    setPos(Rando.randInt(0, maxId), Rando.randDouble(0., 1.));
  }
  
  public void setPos(int seg, double ratio) {
    this.segId = seg % maxId; // wrap if over max
    this.ratio = ratio;
    List<Vector3f> points = polyline.getPoints();
    int nextId = (segId == maxId)? 0 : (segId + 1);
    lineSegment = Line3d.newStartToEnd(
      points.get(segId).toVector3d(), 
      points.get(nextId).toVector3d());
    length = lineSegment.getLength();
  }
  
  public void setOffset(double offset) {
    offset_r = offset;
    offset_a = Math.PI/2;
  }
  
  public void setOffset(Vector3d offset) {
    offset_r = offset.length();
    offset_a = offset.getAngle() - getTraceAngle();
  }
  
  public void resetAngleOffset() {
    angleOffset = 0;
  }
  
  public void setAngleOffset(double angle) {
    angleOffset = angle - getTraceAngle();
  }
  
  public void walk(double diff) {
    // Update position:
    ratio += diff / length;
    if(ratio > 1.) {
      while(ratio > 1.) {
        ratio -= 1;
        segId = (segId == maxId)? 0 : (segId + 1);
      }
      setPos(segId, ratio);
    } else if(ratio < 0.) {
      while(ratio < 0.) {
        ratio += 1;
        segId = (segId == 0)? maxId : (segId - 1);
      }
      setPos(segId, ratio);
    }
  }
  
  public Vector3d getTracePos() {
    return lineSegment.getPosAt(ratio);
  }
  
  public double getTraceAngle() {
    return lineSegment.getDirection().getAngle();
  }
  
  public Vector3d getPos() {
    return getTracePos().add(new Vector3d(offset_r, offset_a + getTraceAngle()));
  }
  
  public double getAngle() {
    return getTraceAngle() + angleOffset;
  }
}
