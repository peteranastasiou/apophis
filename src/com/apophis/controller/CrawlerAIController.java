
package com.apophis.controller;

import com.apophis.Camera;
import com.apophis.entity.Entity;
import com.apophis.gfx.graphic.PolyTrace;
import com.apophis.utils.Rando;

public final class CrawlerAIController implements Controller {
  private double speed = .2;
  private double angle0, angle1; // a0 is average of [n-1].slope and [n].slope, a1 is average of [n+1].slope and [n].slope
  private long lastUpdated = 0;
  private long nextDecision = 0; // time of next change direction decision
  
  public int state; // -1, 0 or 1: left, still, right
  public int frameTimer;
  
  public CrawlerAIController() {
  }
  
  @Override
  public void control(Entity entity, long time) {
    PolyTrace trace = entity.getPhysics().getTrace();
    if(trace == null) {
      return;
    }
    if(nextDecision == 0) {
      state = Rando.randInt(-1, 1);
      nextDecision = time + Rando.randInt(0, 6000);
    }
    if(state == 0) {
        entity.getGraphic(0).setFrame(0);
    } else {
      if(time % 10 < 5) {
        entity.getGraphic(0).setFrame(1);
      } else {
        entity.getGraphic(0).setFrame(2);
      }
    }
    
    // Make new decision:
    if(time > nextDecision) {
      if(Rando.oneIn(3)) {
        switch(state) {
          case 0:
            if(Rando.oneIn(2)) {
              state = 1;
              entity.getGraphic(0).setFlip(false, false);
            } else {
              state = -1;
              entity.getGraphic(0).setFlip(true, false);
            }
            nextDecision += 1000;
            break;
          case 1:
            state = 0;
            nextDecision += 4000;
            break;
          case -1:
            state = 0;
            nextDecision += 4000;
            break;
        }
      }
    }
    
    // Update position:
    trace.walk(state*speed*(time - lastUpdated));
    lastUpdated = time;
  }

  @Override
  public void render(Entity e, Camera c) {
  }
}
