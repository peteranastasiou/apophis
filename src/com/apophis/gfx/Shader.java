package com.apophis.gfx;

import com.apophis.maths.Matrix4d;
import com.apophis.maths.Vector3f;
import static org.lwjgl.opengl.GL20.*;

import java.util.HashMap;
import java.util.Map;

public class Shader {
  // attribute "locations" in shader programs:
  public static final int VERTEX_ATTRIB   = 0;
  public static final int TCOORD_ATTRIB   = 1;
  public static final int COLOR_ATTRIB    = 2; // TODO color is ambient?!?
  public static final int NORMAL_ATTRIB   = 3;

  private final String name;
  private boolean enabled = false;
  private final int ID;
  private final Map<String, Integer> locationCache;

  public Shader(String name, int id) {
    this.name = name;
    this.ID = id;
    locationCache = new HashMap<>();
  }

  public boolean hasUniform(String uname) {
    if(locationCache.containsKey(uname)) {
      return true;
    }
    return glGetUniformLocation(ID, uname) != -1;
  }
  
  public int getUniform(String uname) {
    if(locationCache.containsKey(uname)) {
      return locationCache.get(uname);
    }

    int result = glGetUniformLocation(ID, uname);
    if(result == -1) {
      System.err.println("Shader '"+name+"' has no uniform called '" + uname + "'!");
    } else {
      locationCache.put(uname, result);
    }
    return result;
  }

  public void setUniform1i(String uname, int value) {
    if(!enabled) {
      enable();
    }
    glUniform1i(getUniform(uname), value);
  }

  public void setUniform1f(String uname, float value) {
    if(!enabled) {
      enable();
    }
    glUniform1f(getUniform(uname), value);
  }

  public void setUniform2f(String uname, float x, float y) {
    if(!enabled) {
      enable();
    }
    glUniform2f(getUniform(uname), x, y);
  }

  public void setUniform3f(String uname, Vector3f vector) {
    if(!enabled) {
      enable();
    }
    glUniform3f(getUniform(uname), vector.x, vector.y, vector.z);
  }
  
  public void setUniform4f(String uname, float[] arr) {
    if(!enabled) {
      enable();
    }
    glUniform4f(getUniform(uname), arr[0], arr[1], arr[2], arr[3]);
  }

  // The double precision matrix will be cast to floats within toFloatBuffer()
  public void setUniformMat4f(String uname, Matrix4d matrix) {
    if(!enabled) {
      enable();
    }
    glUniformMatrix4fv(getUniform(uname), false, matrix.toFloatBuffer()); // edit
  }

  public void enable() {
    glUseProgram(ID);
    enabled = true;
  }

  public void disable() {
    glUseProgram(0);
    enabled = false;
  }

}
