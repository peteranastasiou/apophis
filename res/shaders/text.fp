#version 330 core

layout (location = 0) out vec4 color;

in DATA {
    vec2 tc;
} fs_in;

uniform sampler2D tex;
uniform vec3 fcolor;

void main() {
    vec4 c = texture(tex, fs_in.tc);
    if (c.w < 0.1) {
        discard;
    } else {
        color = vec4(fcolor, 1);
    }
}
