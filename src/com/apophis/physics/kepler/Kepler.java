package com.apophis.physics.kepler;

import com.apophis.maths.Vector3d;
import static java.lang.Math.sqrt;
import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.acos;
import static java.lang.Math.sinh;
import static java.lang.Math.cosh;
import static java.lang.Math.pow;
import static java.lang.Math.PI;
import static java.lang.Math.abs;

// BASED ON EQUATIONS FROM:
// Orbital Mechanics for Engineering Students by Howard D. Curtis
// Specifically Section 3.7 of
// http://projecte-hermes.upc.edu/Enginyeria_Aeroespacial/4A/Enginyeria%20espacial/Teoria/Extra/Orbital%20Mechanics%20for%20Engineering%20Students.pdf
// And Matlab code from Appendix D:
// http://courses.washington.edu/aa31008/AA310TextAppendixD.pdf

public class Kepler {

  // stats on last calc done:
  public static int niter = 0; // number of iterations to converge keplers eqn
  public static boolean isError = false; // if did not converge

  private Kepler() {
  } // just static methods, no instances
  
  //TODO implement Stump Taylor Series Approximations and profile compare
  // Implement from page 184
  
  // Stumpff function S useful for solving Kepler's
  public static double stumpS(double z) {
    if(z > 0) {
      double qz = sqrt(z);
      return (qz - sin(qz)) / pow(qz, 3);
    } else if(z < 0) {
      double nz = sqrt(-z);
      return (sinh(nz) - nz) / pow(nz, 3);
    }
    return 1.0 / 6.0;
  }

  // Stumpff function C useful for solving Kepler's
  public static double stumpC(double z) {
    if(z > 0) {
      return (1 - cos(sqrt(z))) / z;
    } else if(z < 0) {
      return (cosh(sqrt(-z)) - 1) / (-z);
    }
    return 1.0 / 2.0;
  }

  // returns kepler's universal anomaly: x, using Newton's Method given:
  // gravitational coeff: mu
  // time since x=0: dt
  // radial position when x=0: r0
  // radial velocity when x=0: vr0
  // inverse of semimajor axis (a): ai
  // error tolerance: error
  public static double keplerU(double dt, double r0, double vr0, double ai, double mu) {
    long tcalc = System.currentTimeMillis();
    int nmax = 1000; // max allowed iterations
    double error = 1e-8;
    
    // qmu is sqrt mu:
    double qmu = sqrt(mu);
    // z: auxiliary: z = a*x^2
    // starting guess:
    double x = qmu * abs(ai) * dt;
    //Log.log().printf("keplers:%ndt\t%d%nr0\t%.04f%nvr0\t%.04f%nai\t%.04f%nmu\t%.04f%n", (int)dt, r0, vr0, ai, mu);
    //Log.log().printf("guess:\t%.04f%n", x);
    // n: number of iterations til convergence
    int n = 0;
    double ratio = 1;
    while(abs(ratio) > error && n < nmax) {
      n++;
      double c = stumpC(ai * x * x);
      double s = stumpS(ai * x * x);
      double f = r0 * vr0 / qmu * x * x * c + (1 - ai * r0) * x * x * x * s + r0 * x - qmu * dt;
      double dfdx = r0 * vr0 / qmu * x * (1 - ai * x * x * s) + (1 - ai * r0) * x * x * c + r0;
      ratio = f / dfdx;
      x -= ratio;
      //Log.log().println(x);
    }
    //Log.log().printf("niter: %d%n%n", n);
    // save num iterations:
    niter = n;
    isError = niter == nmax;
    // the anomaly:
    return x;
  }

  // returns state position and velocity, given:
  // initial state position and velocity: init, 
  // time since initial state: t
  // gravitational coeff: mu
  // error tolerance: error
  public static StateVec orbitPos(StateVec init, double t, double mu) {
    double r0 = init.r.length();             // scalar radius
    double v0 = init.v.length();             // scalar velocity
    double vr0 = init.r.dot(init.v) / r0;    // initial radial velocity
    double ai = 2.0 / r0 - v0 * v0 / mu;     // inverse semimajor axis
    double x = keplerU(t, r0, vr0, ai, mu);  // kepler anomaly
    int attempts = 0;
    int maxAttempts = 20;
    while(isError && attempts < maxAttempts) { // if didn't converge:
      t ++; // try next time step
      attempts ++;
      x = keplerU(t, r0, vr0, ai, mu); // move point
    }
    if(attempts > 0) {
      if(attempts == maxAttempts) {
        System.out.println("ORBITAL ERROR: NO CONVERGENCE");
      } else {
        //System.out.println("CORRECTED ORBIT: ATTEMPTS = "+attempts);
      }
    }
    
    // find lagrange's f and g:
    double z = ai * x * x;
    double f = 1.0 - x * x / r0 * stumpC(z);
    double g = t - 1.0 / sqrt(mu) * x * x * x * stumpS(z);
    
    // final position vector:
    Vector3d r1 = (init.r.scale(f)).add(init.v.scale(g));
    double r = r1.length();
    
    // find lagrange's time derivative of f and g:
    double fdot = sqrt(mu) / r / r0 * (z * stumpS(z) - 1) * x;
    double gdot = 1.0 - x * x / r * stumpC(z);

    Vector3d v1 = (init.r.scale(fdot)).add(init.v.scale(gdot));
    
    // final State:
    StateVec s = new StateVec(r1, v1);
    s.x = x;
    return s;
  }

  public static OrbitalElements getElements(StateVec st, double mu) {

    double eps = 1e-10;
    double r0 = st.r.length();
    double v0 = st.v.length();
    double vr = st.r.dot(st.v)/r0;

    OrbitalElements oe = new OrbitalElements();

    // angular momentum:
    oe.h = st.r.cross(st.v);
    oe.h0 = oe.h.length();

    // inclination:
    oe.incl = acos(oe.h.z / oe.h0);

    // calculate right ascension of accending node:
    Vector3d n = (new Vector3d(0, 0, 1)).cross(oe.h);
    double n0 = n.length();
    if(n0 != 0.0) {
      oe.ra = acos(n.x / n0);
      if(n.y < 0.0) {
        oe.ra = 2.0 * PI - oe.ra;
      }
    } else {
      oe.ra = 0.0;
    }

    // eccentricity:
    oe.e = ((st.r.scale((v0 * v0 - mu / r0))).add(st.v.scale(-r0 * vr))).scale(1.0 / mu);
    oe.e0 = oe.e.length();

    // argument of perigee:
    if(n0 != 0.0) {
      if(oe.e0 > eps) {
        oe.w = acos(n.dot(oe.e) / n0 / oe.e0);
        if(oe.e.z < 0) {
          oe.w = 2.0 * PI - oe.w;
        }
      } else {
        oe.w = 0.0;
      }
    } else {
      oe.w = 0.0;
    }

    // true anomaly:
    if(oe.e0 > eps) {
      oe.ta = acos(oe.e.dot(st.r) / oe.e0 / r0);
      if(vr < 0.0) {
        oe.ta = 2 * PI - oe.ta;
      }
    } else {
      Vector3d nxr = n.cross(st.r);
      if(nxr.z >= 0) {
        oe.ta = acos(n.dot(st.r) / n0 / r0);
      } else {
        oe.ta = 2 * PI - acos(n.dot(st.r) / n0 / r0);
      }
    }

    // semimajor axis:
    oe.a = oe.h0 * oe.h0 / mu / (1 - oe.e0 * oe.e0);

    oe.isEllipse = oe.e0 < 1.0;
    if(oe.isEllipse) {
      oe.period = (long)(2 * PI / sqrt(mu) * Math.pow(oe.a, 1.5));
    } else {
      oe.period = -1;
    }
    
    // apsides:
    oe.apogee = oe.a*(1.0+oe.e0);
    oe.perigee = oe.a*(1.0-oe.e0);

    return oe;
  }

  public static void testKepler() {

    double mu = 398600;
    //~~~~~~~~~~~~~~~~~~test Keplers~~~~~~~~~~~~~~~~~~
    {
      long dt = 3600; // elapsed s
      double r = 10000; // km
      double vr = 3.0752; // km/s
      double a = -19655;  // km
      System.out.printf("KeplerU:Result = %f (km^.5)\n",
        keplerU(dt, r, vr, 1.0 / a, mu));
      // Expected Universal anomaly (kmˆ0.5) = 128.511

      System.out.println("--------------");
    }
    {
      //~~~~~~~~~~~~~~~~~~test Orbit~~~~~~~~~~~~~~~~~~
      Vector3d ri = new Vector3d(7000, -12124, 0);
      Vector3d vi = new Vector3d(2.6679, 4.6210, 0);
      long t = 3600;
      //initial state:
      StateVec init = new StateVec(ri, vi);
      System.out.println("Initial\n" + init);
      StateVec rv = orbitPos(init, t, mu);
      System.out.println("Final\n" + rv);
      System.out.println("--------------");
      // Expected Final position vector (km):
      // r = (-3297.77, 7413.4, 0)
      // Expected Final velocity vector (km/s):
      // v = (-8.2976, -0.964045, -0)
    }
    {
      //~~~~~~~~~~~~~~~~~~test Elements~~~~~~~~~~~~~~~~~~
      Vector3d ri = new Vector3d(-6045, -3490, 2500);
      Vector3d vi = new Vector3d(-3.457, 6.618, 2.533);
      StateVec init = new StateVec(ri, vi);
      OrbitalElements oe = getElements(init, mu);
      double rad2deg = 180.0 / PI;
      System.out.println("Initial:\n" + init);
      System.out.println("Orbit:" + (oe.isEllipse?"Ellipse":"Hyperbola"));
      System.out.printf("angular momentum: %1.03f\n", oe.h0);
      System.out.printf("eccentricity: %1.03f\n", oe.e0);
      System.out.printf("right ascension: %1.03f\n", oe.ra * rad2deg);
      System.out.printf("inclination: %1.03f\n", oe.incl * rad2deg);
      System.out.printf("argument of perigee: %1.03f\n", oe.w * rad2deg);
      System.out.printf("true anomaly: %1.03f\n", oe.ta * rad2deg);
      System.out.printf("semimajor axis: %1.03f\n", oe.a);
      System.out.printf("period: %1.03f\n", oe.period);

    }
  }

}
