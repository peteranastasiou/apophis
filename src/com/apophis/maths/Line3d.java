package com.apophis.maths;

import com.apophis.gfx.Color;
import com.apophis.gfx.graphic.Polyline;
import java.io.Serializable;

//TODO Seperate Collision stuff to a Manifold class (which is later resolved?)

public final class Line3d implements Serializable {

  private Vector3d pos;
  private Vector3d dir;

  public Line3d() {
  }
  
  public static Line3d newStartToEnd(Vector3d start, Vector3d end) {
    Line3d line = new Line3d();
    line.setStartToEnd(start, end);
    return line;
  }
  
  public static Line3d newStartAndDir(Vector3d start, Vector3d dir) {
    Line3d line = new Line3d();
    line.setStartAndDir(start, dir);
    return line;
  }
  
  public void setStartToEnd(Vector3d start, Vector3d end) {
    this.pos = start;
    this.dir = end.subtract(start);
  }

  public void setStartAndDir(Vector3d start, Vector3d dir) {
    this.pos = start;
    this.dir = dir;
  }

  public Vector3d getStart() {
    return pos;
  }

  public Vector3d getDirection() {
    return dir;
  }

  public Vector3d getEnd() {
    return pos.add(dir);
  }

  public double getLength() {
    return dir.length();
  }
  
  public Vector3d getPosAt(double ratio) {
    return pos.add(dir.scale(ratio));
  }
  
  public void setStart(Vector3d start) {
    pos = start;
  }

  public class CollisionResult {
    public Vector3d intersection;
    public Vector3d removalVector;
    public Vector3d reboundDir;
    public int segId;
    public double t, ratio; // value 0-1 along incoming line where intersect occured
  }

  // Consider two line segments: p->p+r and q->q+s.
  // Find intersection if exists: p->p+tr = q->q+us.
  // Also find rebound direction for p->p+r bouncing off q->q+s
  // for momentum qs is considered static, pr is considered the incoming ray
  public CollisionResult findIntersection(Line3d pr) {
    Vector3d p = pr.pos;
    Vector3d r = pr.dir;
    Vector3d q = this.pos;
    Vector3d s = this.dir;
    
    // Check if moving away from object. Assumes winding is counterclockwise!
    if(r.angleBetween(s) < 0) { // Check if r is counterclockwise from s
      return null;
    }
    
    double rs = r.multItx(s);
    // Check if parallel => not intersecting:
    if(Math.abs(rs) < 0.0001) {
      return null;
    }

    Vector3d pq = q.subtract(p);

    // Check within q->q+s:
    double u = pq.multItx(r) / rs;
    if(u < 0 || u > 1) {
      return null;
    }
    // Check within p->p+r:
    double t = pq.multItx(s) / rs;
    if(t < 0 || t > 1) {
      return null;
    }

    CollisionResult result = new CollisionResult();
    result.intersection = p.add(r.scale(t));
    result.removalVector = r.scale(t - 1); // t-1 gives removal along r
    result.t = t;
    result.ratio = u;
    // TODO REBOUND DIRECTION WITH MOMENTUM AND ANGULAR MOMENTUM:
    //if(pr.getLength() < 1.) {
      result.reboundDir = Vector3d.ZERO;
    //} else {
    //  result.reboundDir = pr.dir.scale(-1.);
   // }
    
    //System.out.println("Intersection @ t= " + t);
    //System.out.println("Removal Vector: " + result.removalVector);
    
    return result;
  }

  public void updatePolyline(Polyline p) {
    p.reset();
    p.addPoint(getStart());
    p.addPoint(getEnd());
    p.setColor(Color.GREEN);
    p.load();
  }

  @Override
  public String toString() {
    return pos.toString() + ", direction:" + dir.toString();
  }
}
