package com.apophis.gfx.graphic;

// Fixed Width (Monospaced) Bitmap Font Handler

import com.apophis.Main;
import com.apophis.gfx.Color;
import com.apophis.gfx.Shader;
import com.apophis.gfx.Texture;
import com.apophis.maths.Matrix4d;
import com.apophis.maths.Vector3d;
import com.apophis.maths.Vector3f;

public class MonoFont {
  private final Graphic graphic;
  private final float width;
  public static final float XSHIFT = 1f/95f;
  private boolean allCaps = false;

  public MonoFont(String fontBitmap, int w, int h) {
    width = 2f*w/Main.WIDTH;
    //System.out.println(0.5/width);
    
    
    graphic = new Graphic();
    graphic.setMesh("font_mesh");
    graphic.setTexture(new Texture(fontBitmap));
    graphic.setShader("text");
    graphic.setScale(1);
  }

  public void setAllCaps(boolean allCaps) {
    this.allCaps = allCaps;
  }
  
  public void printL(String text, Vector3d pos, Color color, float scale) {
    Vector3d origin = new Vector3d(-1, 0.5, 0);
    print(text, pos, origin, color, scale);
  }
  
  public void printC(String text, Vector3d pos, Color color, float scale) {
    double l = text.length()-1;
    //double q = -width*l*.35;
    double p = -l/50;
    Vector3d origin = new Vector3d(p, 0.5, 0);
    print(text, pos, origin, color, scale);
  }
  
  public void printR(String text, Vector3d pos, Color color, float scale) {
    Vector3d origin = new Vector3d(1f-width*text.length(), 0.5, 0);
    print(text, pos, origin, color, scale);
  }
  
  public void print(String text, Vector3d pos, Vector3d origin, Color color, float scale) {
    Shader shader = graphic.getShader();
    if(shader == null) return;
    
    float ratio = (float) Main.HEIGHT / (float) Main.WIDTH;
    Matrix4d pr = Matrix4d.orthographic(
        -1.0f, 1.0f, -1.0f * ratio, 1.0f * ratio, -100.0f, 100.0f);
    
    if(allCaps) text = text.toUpperCase();
    
    shader.enable();
    graphic.getTexture().bind();
    shader.setUniform3f("fcolor", new Vector3f(color.r, color.g, color.b));
    for(int i = 0; i < text.length(); i ++) {
      Matrix4d vw = Matrix4d.translate(origin)
        .multiply(Matrix4d.scale(scale*width))
        .multiply(Matrix4d.translate(pos));
      shader.setUniformMat4f("pr_vw_md", pr.multiply(vw));
      float ci = text.charAt(i)-32;
      if(ci >= 0 && ci < 95) {
        shader.setUniform1f("offset", ci*XSHIFT);
        graphic.getMesh().render();
      }
      pos = pos.add(new Vector3d(1f,0,0));
    }
    shader.disable();
  }
  
}
