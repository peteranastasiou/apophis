package com.apophis.maths;

import com.apophis.utils.BufferUtils;
import java.nio.FloatBuffer;

// Matrices are in column major order:
// access with [row + col * 4]
public class Matrix4d {

  public static final int SIZE = 4 * 4;
  public double[] elements = new double[SIZE];

  public Matrix4d() {
  }

  public static Matrix4d identity() {
    Matrix4d result = new Matrix4d();
    result.elements[0 + 0 * 4] = 1.0;
    result.elements[1 + 1 * 4] = 1.0;
    result.elements[2 + 2 * 4] = 1.0;
    result.elements[3 + 3 * 4] = 1.0;
    return result;
  }

  public static Matrix4d orthographic(double left, double right, double bottom, double top, double near, double far) {
    Matrix4d result = identity();

    result.elements[0 + 0 * 4] = 2.0 / (right - left);
    result.elements[1 + 1 * 4] = 2.0 / (top - bottom);
    result.elements[2 + 2 * 4] = 2.0 / (near - far);
    result.elements[0 + 3 * 4] = (left + right) / (left - right);
    result.elements[1 + 3 * 4] = (bottom + top) / (bottom - top);
    result.elements[2 + 3 * 4] = (far + near) / (far - near);

    return result;
  }

  public static Matrix4d perspective(double fovy, double aspect, double zNear, double zFar) {
    double fovyRad = fovy * (double)Math.PI / 180.0;
    double f = 1.0 / (double)Math.tan(fovyRad / 2.0);
    double zD = zNear - zFar;

    Matrix4d result = new Matrix4d();
    //              r   c 
    result.elements[0 + 0 * 4] = f / aspect;
    result.elements[1 + 1 * 4] = f;
    result.elements[2 + 2 * 4] = (zFar + zNear) / zD;
    result.elements[2 + 3 * 4] = (2.0 * zFar * zNear) / zD;
    result.elements[3 + 2 * 4] = -1.0;

    return result;
  }

  public static Matrix4d translate(Vector3d vector) {
    Matrix4d result = identity();
    result.elements[0 + 3 * 4] = vector.x;
    result.elements[1 + 3 * 4] = vector.y;
    result.elements[2 + 3 * 4] = vector.z;
    return result;
  }

  // rotation anticlockwise in radians:
  public static Matrix4d rotateZ(double angle) {
    Matrix4d result = identity();
    double cos = (double)Math.cos(angle);
    double sin = (double)Math.sin(angle);

    //              r   c
    result.elements[0 + 0 * 4] = cos;
    result.elements[1 + 0 * 4] = sin;
    result.elements[0 + 1 * 4] = -sin;
    result.elements[1 + 1 * 4] = cos;

    return result;
  }

  public static Matrix4d rotateX(double angle) {
    Matrix4d result = identity();
    double cos = (double)Math.cos(angle);
    double sin = (double)Math.sin(angle);

    //              r   c
    result.elements[1 + 1 * 4] = cos;
    result.elements[2 + 1 * 4] = sin;
    result.elements[1 + 2 * 4] = -sin;
    result.elements[2 + 2 * 4] = cos;

    return result;
  }

  public static Matrix4d rotateY(double angle) {
    Matrix4d result = identity();
    double cos = (double)Math.cos(angle);
    double sin = (double)Math.sin(angle);

    //              r   c
    result.elements[0 + 0 * 4] = cos;
    result.elements[0 + 2 * 4] = sin;
    result.elements[2 + 0 * 4] = -sin;
    result.elements[2 + 2 * 4] = cos;

    return result;
  }

  public static Matrix4d scale(double scale) {
    Matrix4d result = new Matrix4d();

    result.elements[0 + 0 * 4] = scale;
    result.elements[1 + 1 * 4] = scale;
    result.elements[2 + 2 * 4] = scale;
    result.elements[3 + 3 * 4] = 1.0;

    return result;
  }

  public static Matrix4d scale(double xs, double ys, double zs) {
    Matrix4d result = new Matrix4d();

    result.elements[0 + 0 * 4] = xs;
    result.elements[1 + 1 * 4] = ys;
    result.elements[2 + 2 * 4] = zs;
    result.elements[3 + 3 * 4] = 1.0;

    return result;
  }

  public Matrix4d multiply(Matrix4d matrix) {
    Matrix4d result = new Matrix4d();
    for(int y = 0; y < 4; y++) {
      for(int x = 0; x < 4; x++) {
        double sum = 0.0;
        for(int e = 0; e < 4; e++) {
          sum += this.elements[x + e * 4] * matrix.elements[e + y * 4];
        }
        result.elements[x + y * 4] = sum;
      }
    }
    return result;
  }

  public Vector3d multiply(Vector3d vec) {
    //          r   c
    double x
      = elements[0 + 0 * 4] * vec.x
      + elements[0 + 1 * 4] * vec.y
      + elements[0 + 2 * 4] * vec.z;
    double y
      = elements[1 + 0 * 4] * vec.x
      + elements[1 + 1 * 4] * vec.y
      + elements[1 + 2 * 4] * vec.z;
    double z
      = elements[2 + 0 * 4] * vec.x
      + elements[2 + 1 * 4] * vec.y
      + elements[2 + 2 * 4] * vec.z;
    return new Vector3d(x, y, z);
  }

  public FloatBuffer toFloatBuffer() {
    // Cast to float array:
    float[] floats = new float[SIZE];
    for(int i = 0; i < SIZE; ++i) {
      floats[i] = (float)elements[i];
    }
    // Create float buffer:
    return BufferUtils.createFloatBuffer(floats);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("{\n");
    for(int r = 0; r < 4; r++) {
      for(int c = 0; c < 4; c++) {
        sb.append(String.format(" %+.2f ", elements[r + c * 4]));
      }
      sb.append('\n');
    }
    sb.append("}\n");
    return sb.toString();
  }
}
