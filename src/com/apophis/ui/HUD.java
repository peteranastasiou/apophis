package com.apophis.ui;

import com.apophis.Const;
import com.apophis.lib.Lib;
import com.apophis.ui.command.Command;
import com.apophis.gfx.Color;
import com.apophis.maths.Vector3d;
import com.apophis.utils.Input;
import java.awt.Point;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HUD implements Serializable {

  private int lineR = 0;
  private int lineL = 0;
  private boolean showGauges;
  private Bar fuelGauge;
  private final Set<Command> commands;
  private final Set<Button> buttons;

  public HUD() {
    showGauges = true;
    //fuelGauge = new Bar(-0.8, -0.8, 0.1, 0.4, 50);
    commands = new HashSet<>();
    buttons = new HashSet<>();
  }
  
  public void reset() {
    lineL = 0;
    lineR = 0;
  }
  
  public void printL(String text) {
    Lib.getFont("fontSmall").printL(text, new Vector3d(5, -lineL, Const.LAYER_OVERLAY), Color.WHITE, 2f);
    lineL ++;
  }
  
  public void printR(String text) {
    Lib.getFont("fontSmall").printL(text, new Vector3d(55, -lineR, Const.LAYER_OVERLAY), Color.WHITE, 2f);
    lineR ++;
  }
  
  public void addButton(Button b) {
    if(!buttons.contains(b)) {
      buttons.add(b);
    }
  }
  
  public void removeButton(Button b) {
    buttons.remove(b);
  }
  
  public void addCommand(Command c) {
    if(!commands.contains(c)) {
      commands.add(c);
    }
  }
  
  public void removeCommand(Command c) {
    commands.remove(c);
  }
  
  public void update() {
    // Update Commands:
    Iterator<Command> i = commands.iterator();
    while(i.hasNext()) {
      Command c = i.next();
      if(Input.isKeyPressed(c.getKey())) {
        c.execute();
      }
      if(!c.isLive()) {
        i.remove();
      }
    }
    // Update the buttons:
    Point mouseDown = Input.wasMouseDown(0);
    if(mouseDown != null) {
      for(Button b : buttons) {
        if(b.invokeMouseDown(mouseDown)) break;
      }
    }
    Point mouseUp = Input.wasMouseUp(0);
    if(mouseUp != null) {
      for(Button b : buttons) {
        b.invokeMouseUp(mouseUp);
      }
    }
  }
  
  public void printCommands() {
    for(Command c : commands) {
      printR(((char)c.getKey())+": "+c.getDescription());
    }
  }
}
