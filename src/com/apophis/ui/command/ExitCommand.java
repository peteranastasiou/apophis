
package com.apophis.ui.command;

import com.apophis.Main;
import com.apophis.entity.Entity;
import com.apophis.maths.Vector3d;
import com.apophis.utils.Rando;
import java.util.Objects;

public class ExitCommand implements Command {
  private final Entity target;
  private final String descr;
  private final int keycode;
  private boolean live;
  
  public ExitCommand(int keycode, Entity target, String descr) {
    this.keycode = keycode;
    this.target = target;
    this.descr = descr;
    live = true;
  }
  
  @Override
  public int getKey() {
    return keycode;
  }

  @Override
  public void execute() {
    // Pop the pilot:
    Entity child = target.popHidden();
    if(child != null) {
      child.setParent(target.getParent().getId());
      child.setPosRel(target.getPosRel());
      child.setAngle(target.getPosRel().getAngle()-Math.PI/2);
      // new velocity
      Vector3d v = target.getVelRel();
      if(v.length() > 0.1) {
        child.setVelRel(v.scale(1.0 - 0.1/v.length()));
      } else {
        child.setVelRel(new Vector3d(0.1, Rando.randDouble(0, 2*Math.PI)));
      }
      // Reboot physics at the new position:
      child.getPhysics().init(child, Main.universe().getTime());
      // Set Focus:
      Main.universe().showEntity(child.getId());
      Main.universe().setFocus(child.getId());
      Main.universe().getCamera().switchToEntity(child);
    }
    live = false;
  }

  @Override
  public boolean isLive() {
    return live;
  }
  
  @Override
  public String getDescription() {
    return descr;
  }
  
  @Override
  public boolean equals(Object obj) {
    if(this == obj) return true;
    if(obj == null) return false;
    if(getClass() != obj.getClass()) return false;
    final ExitCommand other = (ExitCommand)obj;
    if(this.keycode != other.keycode) return false;
    return Objects.equals(this.target, other.target);
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 67 * hash + Objects.hashCode(this.target);
    hash = 67 * hash + this.keycode;
    return hash;
  }
}
