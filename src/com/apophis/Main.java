package com.apophis;

import com.apophis.lib.Lib;
import com.apophis.entity.Entity;
import com.apophis.gfx.Color;
import com.apophis.utils.Input;
import com.apophis.maths.Vector3d;
import com.apophis.ui.Button;
import com.apophis.ui.HUD;
import com.apophis.utils.Log;
import com.apophis.utils.Util;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.system.MemoryUtil.*;

import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

/** TODO list:
 * LAYER TO GRAPHIC, NOT ENTITY!
 * Use kepler function based on anomaly (angle from focus?) rather than time to draw orbits (hyperbolic and eliptic)
 * Fix Star Shader shifting and rotation
 * Rolling Physics! (+Gravity induced roll - angular momentum too!)
 * Save serialize and Reconstruct
 * Fix Collision for changing angle (track dAngle, include angular velocity).
 * Size based on mass from planet template!
 * Templates loaded from text file rules
 * Spheres -> hemispheres! Merge Sphere and Spheroid classes.
 * Clip Orbit drawn to edge of crossover point - done for 1 of 2 cases
 * Collision resultant vector, angular momentum
 * planet detail shader / texture writer
 * 
 *
 * DONE:
 * lighting and normals
 * Enter/exit craft - Entities pushed in and popped out of entity controller craft
 * Re-scale universe (tinier) to get a better speed of orbit for distance. Use ratio of craft current size to actual size
 * Fix crossover issue exiting moon soi
 * Walking system - walk on outline
 * Implement Entity as a final component holder: MOVE getAbsPos to Entity, Entity stores position from update step of physics.
 * Draw land as hemispheres instead! Fix perimeter to calculated edges
 * Shift tab cycle.
 * Time-elapsed fix frame rate - physics efficiency meter?
 * Pixel perfect poly-collisions
 * Move models to Lib. E.g. one high precision circle for re-use
 * CelestialBody and DynamicBody implement "Physics"
 * Collision
 * Max planet distance from center in xy plane -> collision outline
 * Dynamic Polygon updating
 * merger with kepler
 * obj loader
 * texturing
 * Load font image in res as texture. Create Billboard object and display text
 */
public class Main implements Runnable {

  public static final String FILENAME = "SAVEGAME.SER";
  public static final int WIDTH = 1280;
  public static final int HEIGHT = 720;
  private static final int FRAME_DELAY_MS = 16; // Equates to f ~= 60FPS
  
  private static Universe universe;
  public static boolean paused = false;
  
  private Thread thread;
  private boolean running = false;
  private long window;
  
  private Button testBtn;

  public void start() {
    running = true;
    thread = new Thread(this, "Game");
    thread.start(); // starts Main.run
  }
  
  @Override
  public void run() {
    if(!init()) {
      return;
    }
    while(running) {
      long time_ms = System.currentTimeMillis();
      update();
      render();
      if(glfwWindowShouldClose(window) == GL_TRUE) {
        running = false;
      }
      // Calculate ms of time taken and subtract from frame_delay_ms
      long timetaken = System.currentTimeMillis() - time_ms;
      if(timetaken < FRAME_DELAY_MS) {
        // NOTE: Comment out for profiling!
        Util.delayms(FRAME_DELAY_MS - timetaken);
      }
    }
  }
  
  public static HUD getHUD() {
    return universe.getHUD();
  }
  
  public static Universe universe() {
    return universe;
  }

  private boolean init() {
    // GLFW INITIALISATION:
    if(glfwInit() != GL_TRUE) {
      System.err.println("Could not initialize GLFW!");
      return false;
    }
    
    // GLFW HINTS:
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    // enable VSYNC:
    glfwSwapInterval(1);
    // enable 4x multisampling:
    glfwWindowHint(GLFW_STENCIL_BITS, 4);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // Create window:
    window = glfwCreateWindow(WIDTH, HEIGHT, "Game", NULL, NULL);
    if(window == NULL) {
      System.err.println("Could not create GLFW window!");
      return false;
    }
    
    GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    glfwSetWindowPos(window, (vidmode.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 4);
    
    glfwSetKeyCallback(window, Input.keyIn);
    glfwSetCursorPosCallback(window, Input.cursorIn);
    glfwSetMouseButtonCallback(window, Input.mouseIn);

    glfwMakeContextCurrent(window);
    glfwShowWindow(window);

    // OPENGL INIT:
    GL.createCapabilities();

    glEnable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    System.out.println("OpenGL: " + glGetString(GL_VERSION));
    
    if(!Lib.loadResources()) return false;
    
    testBtn = new Button("Hello", WIDTH - 101, HEIGHT - 101, 100, 100);
    
    System.out.println("DONE INITIALISATION\n------\n\n");
    
    return true;
  }

  private void update() {
    glfwPollEvents();
    if(universe == null) {
      if(Input.wasKeyPressed(GLFW_KEY_N)) {
        // New Game:
        universe = new Universe();
        universe.generate(282947);
      } else if(Input.wasKeyPressed(GLFW_KEY_1)) {
        // Restore Slot 1:
        universe = restore();
      }
      if(testBtn.wasClicked()) {
        System.out.println("Clicked button");
      }
      Point mouseDown = Input.wasMouseDown(0);
      Point mouseUp = Input.wasMouseUp(0);
      if(mouseDown != null) testBtn.invokeMouseDown(mouseDown);
      if(mouseUp != null) testBtn.invokeMouseUp(mouseUp);
    } else {
      if(Input.isKeyPressed(GLFW_KEY_S)) {
        Camera cam = universe.getCamera();
        cam.setScale(cam.getScale()*1.03f);
      } 
      if(Input.isKeyPressed(GLFW_KEY_A)) {
        Camera cam = universe.getCamera();
        cam.setScale(cam.getScale()/1.03f);
      }
      if(Input.wasKeyPressed(GLFW_KEY_RIGHT_BRACKET)) {
        universe.incrSimSpeed();
      }
      if(Input.wasKeyPressed(GLFW_KEY_LEFT_BRACKET)) {
        universe.decrSimSpeed();
      }

      Entity entity = universe.getFocused();
      if(!paused) {
        // Update:
        long time = universe.update();
      }
      if(Input.wasKeyPressed(GLFW_KEY_F9)) {
        // reload shaders:
        System.out.println("Reload shaders...");
        Lib.loadResources();
      }
      if(Input.wasKeyPressed(GLFW_KEY_P)) {
        paused = !paused;
      }
      if(Input.wasKeyPressed(GLFW_KEY_TAB)) {
        boolean is_shift = Input.isKeyPressed(GLFW_KEY_LEFT_SHIFT);
        int attempts = 0;
        int max_id = universe.getNextId();
        Entity e = null;
        int focus_id = universe.getFocused().getId();
        while(e == null && attempts < max_id) {
          if(is_shift) {
            focus_id--;
            if(focus_id < 0) {
              focus_id = max_id;
            }
          } else {
            focus_id++;
            if(focus_id > max_id) {
              focus_id = 0;
            }
          }
          e = universe.getFocusable(focus_id);
          attempts ++;
        }
        System.out.println("FOCUS:"+focus_id+" : "+(e!=null?e.getName():"null"));
        universe.setFocus(focus_id);
      }
      if(Input.wasKeyPressed(GLFW_KEY_O)) {
        Const.DRAW_OUTLINES = !Const.DRAW_OUTLINES;
      }
      if(Input.wasKeyPressed(GLFW_KEY_ESCAPE)) {
        save(universe);
        universe = null; // TODO AUTOSAVE
      }
    }
    Input.clear();
  }

  private void render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if(universe == null) {
      int x = 16;
      Lib.getFont("fontBig").printL("  DARK NEBULA", new Vector3d(x, -4, Const.LAYER_OVERLAY), Color.WHITE, 1f);
      int y = -8;
      Lib.getFont("fontBig").printL("<N> START NEW", new Vector3d(x, y--, Const.LAYER_OVERLAY), Color.WHITE, 1f);
      y --;
      for(int i = 1; i <= 8; i ++) {
        File f = new File(FILENAME+i);
        Lib.getFont("fontBig").printL("<"+i+"> LOAD SLOT "+i, 
          new Vector3d(x, y--, Const.LAYER_OVERLAY), f.exists()?Color.WHITE:Color.GRAY, 1f);
      }
      testBtn.render();
    } else {
      if(paused) {
        Lib.getFont("fontBig").printC("PAUSED", new Vector3d(0, 0, Const.LAYER_OVERLAY), Color.WHITE, 1f);
      }
      universe.render();
    }
    glfwSwapBuffers(window);
  }
  
  public static void save(Universe u) {
    try(
      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILENAME+"1"))) {
      out.writeObject(u);
    } catch(Exception e) {
      e.printStackTrace(System.err);
    }
  }

  public static Universe restore() {
    try(
      ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILENAME+"1"))) {
      return (Universe)in.readObject();
    } catch(Exception e) {
      e.printStackTrace(System.err);
    }
    return null;
  }

  public static void main(String[] args) {
    //System.currentTimeMillis()); // classic is 500
    Log.init(new File("log.txt"));
    
    boolean testing = false;
    if(!testing) {
      new Main().start();
    } else {
      new Main().init();
      /*
      // ANGLE BETWEEN TEST:
      // Should print +45 followed by -135
      System.out.println((new Vector3d(1,0,0)).angleBetween(new Vector3d(1,1,0))*180/Math.PI);
      System.out.println((new Vector3d(-1,0,0)).angleBetween(new Vector3d(1,1,0))*180/Math.PI);
      */
      /*
      // COLLISION TEST
      Polyline pl = new Polyline(false);
      pl.addPoint(new Vector3d( 1, 1, 0));
      pl.addPoint(new Vector3d( 1,-1, 0));
      pl.addPoint(new Vector3d(-1,-1, 0));
      pl.addPoint(new Vector3d(-1, 1, 0));
      pl.addPoint(new Vector3d( 1, 1, 0));

      CollisionResult cr = pl.collide(Line3d.newStartToEnd(new Vector3d(0,4,0), new Vector3d(0,0,0)));

      System.out.println(cr.intersection);
      System.out.println(cr.t);
      System.out.println(cr.reboundDir);
      System.out.println(cr.removalVector);
      System.out.println(cr.segId);
      System.out.println(cr.ratio);
      */
      
      // KEPLER TEST:
      //Kepler.testKepler();

      /*
      // INTERSECTION TEST:
      Vector3d a = new Vector3d(-1, 0, 0);
      Vector3d b = new Vector3d( 1, 0, 0);
      Vector3d c = new Vector3d( 1,-1, 0);
      Vector3d d = new Vector3d( 1, 1, 0);

      Line3d ab = new Line3d();
      Line3d cd = new Line3d();

      ab.setStartToEnd(a, b);
      cd.setStartToEnd(c, d);

      Line3d id = ab.findIntersection(cd);

      System.out.println(id);
      */
    }
  }
}
