
package com.apophis.physics;

import com.apophis.Camera;
import com.apophis.entity.Entity;
import com.apophis.gfx.graphic.PolyTrace;
import java.io.Serializable;

public interface Physics extends Serializable {
  public void init(Entity entity, long time);
  public void update(Entity e, long time);
  public void project(Entity e, long time);
  public void render(Entity e, Camera c);
  public void applyDeltaV(Entity e, double dv, double angle, long time);
  public boolean isStatic();
  public PolyTrace getTrace();
}
