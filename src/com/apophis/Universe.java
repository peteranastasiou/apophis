package com.apophis;

import com.apophis.lib.Lib;
import com.apophis.ui.HUD;
import com.apophis.entity.Entity;
import com.apophis.entity.Factory;
import com.apophis.entity.Planet;
import com.apophis.gfx.Color;
import com.apophis.gfx.graphic.Graphic;
import com.apophis.maths.Vector3d;
import com.apophis.utils.Rando;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Peter Anastasiou
 */

public class Universe implements Serializable {
  private static final long MINDELTA = 1;
  private static final int MAX_NUM_ENTITIES = 512;
  
  private long tick = 0;
  private long time;
  private double performance;
  private long deltaTime = MINDELTA;
  
  private final Entity[] entities = new Entity[MAX_NUM_ENTITIES];
  private final List<Integer> planetoids = new ArrayList<>(20);
  private final List<Integer> actors = new ArrayList<>(126);
  private final LinkedList<Integer> freeIds = new LinkedList<>();
  private ListIterator<Integer> actorItr;
  private final Graphic backdrop;
  private final HUD hud;
  private int nextId = 0;
  private int focusId;
  private Camera camera;

  public Universe() {
    backdrop = new Graphic();
    backdrop.setMesh("square");
    backdrop.setShader("starnest");
    backdrop.setLayer(Const.LAYER_BACKGROUND);
    hud = new HUD();
  }
  
  public long getTick() {
    return tick;
  }
  
  public long getTime() {
    return time;
  }
  
  public HUD getHUD() {
    return hud;
  }
  
  public void resetTimeSpeed() {
    deltaTime = MINDELTA;
  }
  
  public int getNextId(){
    return nextId;
  }
  
  private int allocateId() {
    if(!freeIds.isEmpty()) {
      return freeIds.removeFirst();
    } else {
      return nextId++;
    }
  }
  
  public void addPlanet(Entity e) {
    int id = allocateId();
    e.setId(id);
    entities[id] = e;
    planetoids.add(id);
  }
  
  public void addEntity(Entity e) {
    int id = allocateId();
    e.setId(id);
    entities[id] = e;
    System.out.println("Added: "+e);
    showEntity(id);
  }
  
  public void removeEntity(Entity e) {
    e.kill();
  }
  
  public void showEntity(int id) {
    if(actorItr == null) {
      // Not looping so can add to back of arraylist
      actors.add(id);
    } else {
      // Currently looping over actors, need to add with the iterator to avoid
      // Concurrent Modification Error
      actorItr.add(id);
    }
  }
  
  // Can only be called outside of the entity update loop
  // due to concurrent modification error!
  public void hideEntity(int id) {
    actors.remove(new Integer(id));
  }
  
  public Entity get(int id) {
    if(id < 0) {
      return null;
    }
    return entities[id];
  }
  
  public Entity getFocusable(int id) {
    Entity e = entities[id];
    if(e == null) return null;
    if(e.getPlanet() == null && e.getController() == null) return null;
    if(!actors.contains(id) &&
        !planetoids.contains(id)) return null;
    return e;
  }
  
  public Camera getCamera() {
    return camera;
  }
  
  public Entity getFocused() {
    return get(focusId);
  }
  
  public void setFocus(int id) {
    focusId = id;
    camera.switchToEntity(get(id));
  }
  
  // returns body exerting an influence at that point:
  public Entity getInfluence(Vector3d pos) {
    if(planetoids.isEmpty()) {
      return null;
    }
    int min = planetoids.get(0);
    double minr = Double.POSITIVE_INFINITY;
    for(int i : planetoids) {
      Entity e = get(i);
      Planet p = e.getPlanet();
      if(e.withinInfluence(pos) && p.r_soi < minr) {
        min = i;
        minr = p.r_soi;
      }
    }
    return get(min);
  }
  
  public Entity getProjectedInfluence(Vector3d pos) {
    if(planetoids.isEmpty()) {
      return null;
    }
    int min = planetoids.get(0);
    double minr = Double.POSITIVE_INFINITY;
    for(int i : planetoids) {
      Entity e = get(i);
      Planet p = e.getPlanet();
      if(e.withinProjInfluence(pos) && p.r_soi < minr) {
        min = i;
        minr = p.r_soi;
      }
    }
    return get(min);
  }
  public void incrSimSpeed() {
    deltaTime *= 2;
    //if(deltaTime > 32768) {
    //  deltaTime = 32768;
    //}
  }
  
  public void decrSimSpeed() {
    deltaTime /= 2;
    if(deltaTime < MINDELTA) {
      deltaTime = MINDELTA;
    }
  }
 
  public long update() {
    tick++;
    time += deltaTime; // seconds
    for(int i : planetoids) {
      get(i).updatePhysics(time);
    }
    // Use iterator to delete dead entities in the loop
    actorItr = actors.listIterator();
    while(actorItr.hasNext()) {
      int id = actorItr.next();
      Entity e = get(id);
      if(e.isAlive()) {
        // Update the entity:
        e.updatePhysics(time);
        e.control(time);
      } else {
        // Delete entity:
        entities[id] = null;
        // Remove it from the actor list:
        actorItr.remove();
        // Can now reuse id!
        freeIds.add(id);
      }
    }
    actorItr = null;
    // Update UI Commands:
    hud.update();
    // Update Camera:
    camera.update();
    return time;
  }

  public void project(long time) {
    for(int i : planetoids) {
      Entity e = get(i);
      e.getPhysics().project(e, time);
    }
  }
  
  public void generate(long seed) {
    Rando.setSeed(seed);
    
    time = 515795l;
    
    int sun = Factory.makePlanet(this, "sun", -1, time);
    
    int earth = Factory.makePlanet(this, "earth", sun, time);
    int segId = Rando.randInt(0, 2000); // okay if over max segment, wraps
    Factory.makeCraft(this, null, earth, segId, time);
    int hero = Factory.makeHero(this, null, earth, segId, time);
    for(int i = 0; i < 1; i ++) {
      Factory.makeMob(this, "mob", earth, time);
    }
    for(int i = 0; i < 40; i ++) {
      switch(i % 5) {
        case 0:
          Factory.makeDecoration(this, "plantGreen1", earth, time);
          break;
        case 1:
          Factory.makeDecoration(this, "mushroomRed", earth, time);
          break;
        case 2:
          Factory.makeDecoration(this, "grass", earth, time);
          break;
        case 3:
          Factory.makeDecoration(this, "bush", earth, time);
          break;
        case 4:
          Factory.makeDecoration(this, "mushroomBrown", earth, time);
          break;
      }
    }
    
    int moon = Factory.makePlanet(this, "moon", earth, time);
    for(int i = 0; i < 1; i ++) {
      Factory.makeMob(this, "mob", moon, time);
    }
    for(int i = 0; i < 10; i ++) {
      Factory.makeDecoration(this, "plantDark4", moon, time);
      Factory.makeDecoration(this, "plantPurple", moon, time);
    }
    
    int mars = Factory.makePlanet(this, "mars", sun, time);
    for(int i = 0; i < 10; i ++) {
      Factory.makeDecoration(this, "plantRed5", mars, time);
      Factory.makeDecoration(this, "rock", mars, time);
    }
    
    focusId = hero;
    camera = new Camera(get(focusId), .005);
  }
  
  public void render() {
    
    // Update animated shaders:
    Lib.getShader("plume").setUniform1f("time", tick);
    Lib.getShader("plume").disable();
    
    // Performance meter:
    String pbar = "---";
    int pi = (int)Math.round(100.*performance);
    for(int i = 0; i < 100; i += 10) {
      if(pi > i) {
        pbar += '-';
      } else {
        pbar += '.';
      }
    }
    // Draw left 
    hud.printL(get(focusId).getName());
    //if(focused.getParent() != null) {
    //  HUD.printL(focused.getParent().getName());
    //}
    if(Main.universe().deltaTime > 1) {
      Lib.getFont("fontBig").printC("TIMEx"+Main.universe().deltaTime, new Vector3d(0, 0, Const.LAYER_OVERLAY), Color.WHITE, 1f);
    }
    //HUD.printL("TIME:   "+Main.getUniverse().deltaTime+"x");
    //HUD.printL(pbar);
    //HUD.printL(Main.getUniverse().getTimeString());
    //HUD.printL(String.format("Scale %.4f", view.getScale()));
    
    // TODO Must render in order of back to front for displaying alpha properly!
    backdrop.render(null, Vector3d.ZERO, 0);
    // TODO Only render within view (use grid registration? or view-window registration?)
    for(int id : planetoids) {
      get(id).render(camera);
    }
    for(int id : actors) {
      get(id).render(camera);
    }
    // If the focused entity can display controls:
    if(get(focusId).getController() != null) {
      get(focusId).getController().render(getFocused(), camera);
    }
    
    
    // Draw HUD Text:
    if(Main.paused) {
      hud.printR("Controls:");
      hud.printR("arrows: ship control");
      hud.printR("A: zoom in");
      hud.printR("S: zoom out");
      hud.printR("[: slow time");
      hud.printR("]: speed up time");
      hud.printR("TAB: cycle focus");
    }
    //hud.printR("HOME: focus ship");
    hud.printR("ESC: pause");
    
    //Physics physics = focused.getPhysics();
    //if(!physics.isStatic()) {
    //  hud.printL(physics.toString());
    //}
    
    hud.printCommands();
    
    hud.reset();
  }

  public String getTimeString() {
    long t = time/MINDELTA; // sim runs at 64 times speed
    long year = t/Const.SEC_PER_YEAR;
    t = t - year*Const.SEC_PER_YEAR;
    long day = t/Const.SEC_PER_DAY;
    t = t - day*Const.SEC_PER_DAY;
    long hour = t/Const.SEC_PER_HOUR;
    t = t - hour*Const.SEC_PER_HOUR;
    long min = t/Const.SEC_PER_MINUTE;
    long sec = t - min*Const.SEC_PER_MINUTE;
    return String.format("%04d.%02d %02d:%02d", year, day, hour, min, sec);
  }

  public void recordPerformance(double p) {
    if(performance == 0)  {
      performance = p;
    } else {
      performance = 0.1*p+0.99*performance; // weighted moving average
    }
  }
}
