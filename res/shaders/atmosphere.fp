
#version 330 core

in vec2 vtc;
in float angle;

void main(void) {
    vec2 uv = vtc;

    uv.x -= .5; // x,y now range from -0.5 to 0.5
    uv.y -= .5;

    float dist = 0.57;

    float r = distance(uv * 2.2, vec2(0., 0.));
    r = (r - dist) / (1.0 - dist);
    if (r < 0) {
        discard;
    }
    vec4 c0 = vec4(0., 0., 0., 0.);
    vec4 c1 = vec4(.0, .6, 1., 1.); // Purple sky: .4,.2,1.,1. // blue sky: .0,.4,.8,1.

    // Occlusion caused by the planet
    float occ = 1.;
    // The x coord is rotated by angle and offset (to start to fade early)
    float x = uv.x*cos(angle)-uv.y*sin(angle) + 0.2;
    if(x > 0) occ = max(0, 1 - 8*x*x);
    gl_FragColor = mix(c0, c1, occ * (1 - r * r));
}
