package com.apophis.gfx.graphic;

import com.apophis.Camera;
import com.apophis.gfx.Color;
import com.apophis.gfx.Mesh;
import com.apophis.maths.Line3d;
import com.apophis.maths.Line3d.CollisionResult;
import com.apophis.maths.Matrix4d;
import java.util.ArrayList;
import com.apophis.maths.Vector3d;
import com.apophis.maths.Vector3f;
import java.awt.geom.Path2D;
import java.io.Serializable;
import java.util.List;
import static org.lwjgl.opengl.GL11.*;

public class Polyline implements Serializable {
  private final ArrayList<Vector3f> pointList;
  private final Graphic graphic;
  private boolean dynamic = false;
  private boolean loaded = false;
  private transient boolean init = false;
  private Color color;

  public static Polyline newCircle(double radius, double steps) {
    Polyline p = new Polyline(false);
    double a = 0;
    for(int i = 0; i <= steps; i ++) {
      a += 2 * Math.PI / steps;
      p.addPoint(new Vector3d(radius*Math.cos(a), radius*Math.sin(a), 0.));
    }
    return p;
  }

  public Polyline(boolean dynamic) {
    pointList = new ArrayList<>();
    graphic = new Graphic();
    graphic.setShader("flat");
    this.dynamic = dynamic;
    color = new Color(1f,1f,1f);
  }
  
  public void setColor(Color c) {
    color = c.copy();
  }
  
  public Color getColor() {
    return color;
  }
  
  public boolean isLoaded() {
    return loaded;
  }

  public void addPoint(Vector3d pt) {
    if(pt != null) {
      pointList.add(pt.toVector3f());
    }
  }
  
  public void addPoint(Vector3f pt) {
    if(pt != null) {
      pointList.add(pt);
    }
  }

  public List<Vector3f> getPoints() {
    return pointList;
  }
  
  public void reset() {
    pointList.clear();
  }

  public boolean load() {
    float[] vertices = new float[3 * pointList.size()];

    for(int i = 0; i < pointList.size(); i++) {
      vertices[i * 3 + 0] = pointList.get(i).x;
      vertices[i * 3 + 1] = pointList.get(i).y;
      vertices[i * 3 + 2] = pointList.get(i).z;
    }
    int size = pointList.size();
    
    if(!init || graphic.getMesh() == null) {
      // initial load:
      Mesh mesh = new Mesh(size, dynamic);
      mesh.loadVertexBuffer(vertices);
      mesh.setMode(GL_LINE_STRIP);
      mesh.setCount(size); // draw count may be less than vbo size
      graphic.setMesh(mesh);
      graphic.setScale(1.0);
      init = true;
      
    } else if(dynamic) {
      // Re-load data in place:
      Mesh mesh = graphic.getMesh();
      mesh.reloadVertexBuffer(vertices);
      mesh.setCount(size); // draw count may be less than vbo size
      
    } else {
      System.err.println("Polyline: Illegal reload to non-dynamic polyline");
      return false;
    }
    loaded = true;
    return true;
  }
  
  public Path2D toPath2D() {
    Path2D path = new Path2D.Double();
    Vector3f start = pointList.get(0);
    path.moveTo(start.x, start.y);
    for(int i = 1; i < pointList.size(); i++) {
      Vector3f next = pointList.get(i);
      path.lineTo(next.x, next.y);
    }
    path.closePath();
    return path;
  }

  // Point-wise collision:
  public CollisionResult collide(Line3d incoming) {
    Vector3f start;
    Vector3f end;
    Line3d segment = new Line3d();
    // For all segments
    for(int i = 0; i < pointList.size()-1; i++) {
      start = pointList.get(i);
      end = pointList.get(i+1);
      segment.setStartToEnd(start.toVector3d(), end.toVector3d());
      
      CollisionResult result = segment.findIntersection(incoming);
      if(result != null) {
        result.segId = i;
        return result;
      }
    }
    return null;
  }
  
  // Polygon collision:
  public CollisionResult collide(Line3d incoming, Polyline poly, float angle) {
    Vector3d centerStart = incoming.getStart();
    Vector3d centerEnd = incoming.getEnd();
    List<Vector3f> polyPts = poly.getPoints();
    // Iterate the points of the polygon:
    // Keep the collision with the lowest t-value (intersection ratio).
    // This is the collision which occured closest to the start point i.e. first
    // (t is 0 at the start point t is 1 at the end point)
    double tmin = 2.;
    CollisionResult result = null;
    Matrix4d mat0 = Matrix4d.rotateZ(angle)
      .multiply(Matrix4d.scale((float)poly.getScale()));
    //Matrix4f mat1 = Matrix4f.rotateZ(angle2)
    //  .multiply(Matrix4f.scale((float)poly.getScale()));
    for(Vector3f v : polyPts) {
      Vector3d offset = mat0.multiply(v.toVector3d());
      Vector3d start = centerStart.add(offset);
      Vector3d end = centerEnd.add(offset);
      // extend start backwards to avoid small errors:
      start = start.add(start.subtract(end).normalise());
      CollisionResult r = collide(Line3d.newStartToEnd(start, end));
      // If collision occured:
      if(r != null) {
        // Use minimum t-value, this is the foremost intersected point:
        if(r.t < tmin) {
          tmin = r.t;
          result = r;
        }
      }
    }
    /*
    if(result != null) {
      System.out.println("POLY-INTERSECTION at t = "+result.t);
    }
    */
    return result;
  }

  public void setScale(double scale) {
    graphic.setScale(scale);
  }

  public double getScale() {
    return graphic.getScale();
  }

  public void setMode(int mode) {
    graphic.setMode(mode);
  }

  /**
   * render this graphic at position [pos], with angle [angle], on layer [layer].
   * the viewer gives the current position and angular offset.
   * @param camera
   * @param pos
   * @param angle
   * @param layer 
   */
  public void render(Camera camera, Vector3d pos, double angle, int layer) {
    if(loaded) {
      graphic.getShader().setUniform4f("color", color.to4f());
      graphic.setLayer(layer);
      graphic.render(camera, pos, angle);
    }
  }
}
