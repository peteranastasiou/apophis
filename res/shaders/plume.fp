precision mediump float;

// time in ms
uniform float time;

in DATA {
    vec2 tc;
} fs_in;

float wobbly (float x) {
    return  0.2*sin(x)+0.8*sin(x/2.);
}

void main(void) {

  vec2 uv = fs_in.tc;

  if(uv.x > 0.5) {
    discard;
  }

  uv.x -= .9;
  uv.y -= .5;
  float s = .9*time+10.*uv.x+5.*uv.y;
  uv.y *= 2.;
  float r = distance(uv, vec2(0.,0.));
  r -= .016*wobbly(s);
    /*
  vec4 c1 = vec4(.8,1.,1.,1.);
  vec4 c2 = vec4(.5,.9,1.,1.);
  vec4 c3 = vec4(0.,.5,.8,1.);
  vec4 cb = vec4(0.,.5,.8,0.);
    */
  vec4 c1 = vec4(1.,1.,1.,1.);
  vec4 c2 = vec4(1.,1.,.9,1.);
  vec4 c3 = vec4(.8,.8,.5,1.);
  vec4 cb = vec4(.8,.8,.5,0.);
  vec4 c;
  if(r < 0.4) {
    c = mix(c1, c2, r/.4);
  } else if(r < 0.5) {
    c = mix(c2, c3, (r-.4)/.1);
  } else {
    c = mix(c3, cb, (r-.5)/.3);
  }
  gl_FragColor = c;
}
