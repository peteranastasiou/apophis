package com.apophis.entity;

import com.apophis.Const;
import com.apophis.gfx.Color;
import com.apophis.gfx.ColorScheme;
import com.apophis.gfx.graphic.Graphic;
import com.apophis.gfx.graphic.Polyline;
import com.apophis.gfx.graphic.Sphere;
import com.apophis.gfx.graphic.Spheroid;
import com.apophis.maths.Vector3d;
import java.io.Serializable;

// Struct containing planet related stuff
public class Planet implements Serializable {

  public double mu; // gravitational variable
  public double r_soi; // radius of sphere of influence
  public double r_atm; // radius of atmosphere
  public double r_sea; // radius of sea level

  // Save Spheroid parameters which are used to regenerate on deserialize:
  private final Template template;

  public Planet(Template template) {
    // Save for later restoration
    this.template = template;
  }

  /**
   * Planet can regenerate the entity graphics from the template without having
   * to serialize the meshes! (Note: contents is transient within Res so the dud
   * Res.Mesh and Res.Polyline in entity will be clobbered by the below.)
   * @param entity
   */
  public void generate(Entity entity) {
    // do the graphics generation
    int style = (int)template.get("style");
    double scale = (double)template.get("scale");

    // Position is used for the random seed!!
    Vector3d posSeed = (Vector3d)template.get("pos");

    ColorScheme cols;
    float bumpiness, smoothness;
    switch(style) {
      case 0:
        cols = ColorScheme.greenLand;
        bumpiness = 0.5f;
        smoothness = 1;
        break;
      case 1:
        cols = ColorScheme.grayLand;
        bumpiness = 1f;
        smoothness = 1;
        break;
      case 2:
        cols = ColorScheme.aridLand;
        bumpiness = 0.7f;
        smoothness = 0.7f;
        break;
      case 3:
      default:
        cols = ColorScheme.gasLand;
        bumpiness = 0.5f;
        smoothness = 1;
        break;
      case 4:
        cols = ColorScheme.sun;
        bumpiness = 0f;
        smoothness = 0.5f;
        break;
    }

    Graphic g;
    Spheroid sphere = new Spheroid(posSeed, cols, bumpiness, smoothness, scale);
    g = sphere.getGraphic();
    if(style == 4) {
      g.setShader("color");
    } else {
      g.setShader("color_lit");
    }
    g.setScale(scale);
    g.setMaterial(new Vector3d(0.05, 1, 0.2));
    entity.addGraphic(g);

    if(style == 0) {
      // Graphic no. 2: the water:
      g = new Graphic();
      g.setMesh(Sphere.newHemisphere(posSeed, ColorScheme.blueWater));
      g.setShader("color_lit");
      g.setScale(.925 * scale);
      g.setMaterial(new Vector3d(0.05, 1, 0.7));
      entity.addGraphic(g);
      entity.getPlanet().r_sea = .925 * scale;
    }
    if(style == 0) {
      // Graphic no. 3: the atmosphere:
      g = new Graphic();
      g.setMesh("square");
      g.setShader("atmosphere");
      g.setScale(1.5 * scale);
      g.setLayer(Const.LAYER_ATMOSPHERE);
      entity.addGraphic(g);
      entity.getPlanet().r_atm = 1.5 * scale;
    }

    Polyline outline = sphere.getEdgePolyline();
    outline.setColor(Color.WHITE);
    outline.load();
    entity.setOutline(outline);
  }
}
