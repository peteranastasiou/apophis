
package com.apophis.ui.command;

import com.apophis.Main;
import com.apophis.entity.Entity;
import java.util.Objects;

public class EntryCommand implements Command {
  private final Entity child;
  private final Entity dest;
  private final String descr;
  private final int keycode;
  private boolean live;
  
  public EntryCommand(int keycode, Entity child, Entity dest, String descr) {
    this.keycode = keycode;
    this.child = child;
    this.dest = dest;
    this.descr = descr;
    live = true;
  }
  
  @Override
  public int getKey() {
    return keycode;
  }

  @Override
  public void execute() {
    dest.pushHidden(child);
    Main.universe().hideEntity(child.getId());
    Main.universe().setFocus(dest.getId());
    Main.universe().getCamera().switchToEntity(dest);
    live = false;
  }

  @Override
  public boolean isLive() {
    return live;
  }
  
  @Override
  public String getDescription() {
    return descr;
  }
  
  @Override
  public int hashCode() {
    int hash = 5;
    hash = 59 * hash + Objects.hashCode(this.child);
    hash = 59 * hash + Objects.hashCode(this.dest);
    hash = 59 * hash + this.keycode;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if(this == obj) return true;
    if(obj == null) return false;
    if(getClass() != obj.getClass()) return false;
    final EntryCommand other = (EntryCommand)obj;
    if(this.keycode != other.keycode) return false;
    if(!Objects.equals(this.child, other.child)) return false;
    return Objects.equals(this.dest, other.dest);
  }
}
