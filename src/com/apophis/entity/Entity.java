package com.apophis.entity;

import com.apophis.Const;
import com.apophis.Main;
import com.apophis.lib.Lib;
import com.apophis.lib.Res;
import com.apophis.Camera;
import com.apophis.controller.Controller;
import com.apophis.gfx.graphic.Graphic;
import com.apophis.gfx.graphic.Polyline;
import com.apophis.maths.Vector3d;
import com.apophis.physics.Physics;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Peter Anastasiou
 */

public final class Entity implements Serializable {
   // Identity:
  private int id;
  private final String template;
  private String name;
  private boolean alive = true;
  
  // Components:
  private Physics physics;
  private Controller controller;
  private final List<Graphic> graphics;
  private Planet planet;
  
  // Data:
  private int parentId = -1;
  private final List<Entity> children; // TODO change to <long> ids
  private final List<Entity> hidden; // TODO change to <long> ids
  private Vector3d posRel, posAbs; // relative, absolute position
  private Vector3d velRel, velAbs; // relative, absolute velocity
  public Vector3d posProjAbs; // projected absolute position
  private double angle, mass;
  private Res<Polyline> outline, symbol;
  private double viewDistance = .1;
  private boolean viewFollows = false;
  
  public Entity(String template) {
    name = "no-name";
    id = -2; // not allocated yet
    this.template = template;
    graphics = new LinkedList<>();
    children = new LinkedList<>();
    hidden = new LinkedList<>();
    setPosRel(Vector3d.ZERO);
    setVelRel(Vector3d.ZERO);
    setPosProjRel(Vector3d.ZERO);
    alive = true;
  }
  
  // on deserialisation, let planet fix the graphics!
  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
      
    if(planet != null) {
      // Re-generate the unique meshes which were not serialised in 
      // Res<Mesh> and Res<Polyline>:
      planet.generate(this);
    }
  }
  
  // IDENTITY METHODS
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

  public boolean isAlive() {
    return alive;
  }
  
  public void kill() {
    alive = false;
  }
  
  public int getId() {
    return id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public int getParentId() {
    return parentId;
  }
  
  public Entity getParent() {
    return Main.universe().get(parentId);
  }
  
  public void setParent(int parent) {
    if(parent == id) {
      throw new RuntimeException("Can't set parent as self:\n" + toString());
    }
    this.parentId = parent;
  }
  
  // COMPONENTS
  // - PHYSICS
  
  public Physics getPhysics() {
    return physics;
  }
  
  public void setPhysics(Physics physics) {
    this.physics = physics;
  }
  
  // - GRAPHICS
  
  public int numGraphics() {
    return graphics.size();
  }
  
  public Graphic getGraphic(int i) {
    if(i >= graphics.size()) i = graphics.size()-1;
    return graphics.get(i);
  }
  
  public void addGraphic(Graphic graphic) {
    graphics.add(graphic);
  }
  
  // - OUTLINE
  
  public Polyline getOutline() {
    if(outline == null) return null;
    Polyline pOutline = outline.get();
    pOutline.setScale(graphics.get(0).getScale());
    return pOutline;
  }
  
  public Res<Polyline> getOutlineRes() {
    return outline;
  }
  
  // set the outline to a shared library resource
  public void setOutline(Polyline outline) {
    if(outline == null) {
      System.err.println("Null outline in entity "+toString());
      return;
    }
    this.outline = new Res<>(outline);
  }
  
  // set the outline to a unique (e.g. generated) object
  public void setOutline(String outlineKey) {
    this.outline = new Res<>(Lib.POLYLINE, outlineKey);
  }
  
  // - SYMBOL
  
  // set the symbol to a shared library resource
  public void setSymbol(String symbol, double viewDistance) {
    this.symbol = new Res<>(Lib.POLYLINE, symbol);
    this.viewDistance = viewDistance;
  }
  
  public Polyline getSymbol() {
    if(symbol == null) return null;
    return symbol.get();
  }
  
  // - CONTROLLER
  
  public Controller getController() {
    return controller;
  }
  
  public void setController(Controller controller) {
    this.controller = controller;
  }
  
  // - Planet
  
  public Planet getPlanet() {
    return planet;
  }
  
  public void setPlanet(Planet planet) {
    this.planet = planet;
  }
  
  // VIEW SETTINGS
  
  public void setViewFollows(boolean follow) {
    viewFollows = follow; // this determines which view angle to prefer
  }
  
  public double getViewDistance() {
    return viewDistance;
  }
  
  public double getViewAngle() {
    if(viewFollows) return angle;
    else return posRel.getAngle();
  }
  
  // CHILDREN
  
  public void pushChild(Entity entity) {
    children.add(entity);
  }
  
  public Entity popChild() {
    if(!children.isEmpty()) {
      Entity entity = children.remove(children.size()-1);
      return entity;
    }
    return null;
  }
  
  public List<Entity> getChildren() {
    return children;
  }
  
  public void pushHidden(Entity entity) {
    hidden.add(entity);
  }
  
  public Entity popHidden() {
    if(!hidden.isEmpty()) {
      Entity entity = hidden.remove(hidden.size()-1);
      return entity;
    }
    return null;
  }
  
  public List<Entity> getHidden() {
    return hidden;
  }
  
  // PHYSICAL STATE (DATA SHARED BETWEEN COMPONENTS)
  
  public double getAngle() {
    return angle;
  }
  
  public void setAngle(double angle) {
    this.angle = angle;
  }
  
  public double getMass() {
    return mass;
  }
  
  public void setMass(double mass) {
    this.mass = mass;
    if(planet != null) {
      planet.mu = mass*Const.G;
    }
  }
  
  public Vector3d getPosRel() {
    return posRel;
  }
  
  public Vector3d getPosAbs() {
    if(posAbs == null) {
      updatePosition();
    }
    return posAbs;
  }
  
  public void setPosRel(Vector3d posRel) {
    this.posRel = posRel;
    // Update absolute position:
    if(parentId != -1) {
      posAbs = posRel.add(Main.universe().get(parentId).posAbs);
    } else {
      posAbs = posRel;
    }
  }
  
  public Vector3d getVelRel() {
    return velRel;
  }
  
  public Vector3d getVelAbs() {
    if(posAbs == null) {
      updatePosition();
    }
    return velAbs;
  }
  
  public void setVelRel(Vector3d velRel) {
    this.velRel = velRel;
    // Update absolute velocity:
    if(parentId != -1) {
      velAbs = velRel.add(Main.universe().get(parentId).velAbs);
    } else {
      velAbs = velRel;
    }
  }
  
  public Vector3d getPosProjAbs() {
    return posProjAbs;
  }

  public void setPosProjAbs(Vector3d posProjAbs) {
    this.posProjAbs = posProjAbs;
  }
  
  public void setPosProjRel(Vector3d posProjRel) {
    if(parentId != -1) {
      this.posProjAbs = posProjRel.add(Main.universe().get(parentId).getPosProjAbs());
    } else {
      this.posProjAbs = posProjRel;
    }
  }
  
  // PLANET RELATED STUFF
  
  public boolean withinInfluence(Vector3d r) {
    if(planet == null) {
      System.err.println("withinInfluence called on not a planet: "+toString());
      return false;
    }
    return r.subtract(getPosAbs()).length() < planet.r_soi;
  }

  public boolean withinProjInfluence(Vector3d r) {
    if(planet == null) {
      System.err.println("withinInfluence called on not a planet: "+toString());
      return false;
    }
    return r.subtract(getPosProjAbs()).length() < planet.r_soi;
  }
  
  // UPDATE FUNCTIONS
  
  public void control(long time) {
    if(controller != null) controller.control(this, time);
    
    // Pass control to children:
    for(Entity e : children) e.control(time);
  }
  
  public void updatePosition() {
    // Update absolute position and velocity:
    if(parentId != -1) {
      velAbs = velRel.add(Main.universe().get(parentId).velAbs);
      posAbs = posRel.add(Main.universe().get(parentId).posAbs);
    } else {
      velAbs = velRel;
      posAbs = posRel;
    }
  }
  
  public void updatePhysics(long time) {
    // Update relative position and velocity:
    if(physics != null) {
      physics.update(this, time);
    } else {
      // Update absolute position and velocity:
      updatePosition();
    }
    
    // Update children if any:
    for(Entity e : children) e.updatePhysics(time);
  }
  
  public void render(Camera camera) {
    // Physics can render orbit path:
    if(physics != null) physics.render(this, camera);
    
    if(camera.getScale() < viewDistance) {
      // draw graphics:
      for(Graphic graphic : graphics) {
        // Set uniform lighting normal if required (sprites only):
        if(graphic.getShader().hasUniform("unormal")) {
           Vector3d unormal = getPosRel().normalise();
           graphic.setLightingNormal(unormal);
        }
        graphic.render(camera, posAbs, angle);
      }
      Polyline pOutline = getOutline();
      if(pOutline != null && Const.DRAW_OUTLINES) {
        pOutline.render(camera, posAbs, angle, Const.LAYER_OVERLAY);
      }
    } else {
      // draw symbol for "Map" mode:
      Polyline pSymbol = getSymbol();
      if(pSymbol != null) {
        pSymbol.setScale(60*camera.getScale());
        pSymbol.render(camera, posAbs, angle, Const.LAYER_OVERLAY);
      }
    }
    
    // Sphere of influence:
    if(planet != null && camera.getScale() > Const.VIEW_SCALE_ORBIT) {
      Polyline circle = Lib.getPolyline("big_circle");
      circle.setScale(planet.r_soi);
      circle.render(camera, posAbs, angle, Const.LAYER_UNDERLAY);
    }
    
    // Render children:
    for(Entity e : children) e.render(camera);
  }
  
  @Override
  public String toString() {
    return template+" : "+name + " ("+id+")";
  }
}
