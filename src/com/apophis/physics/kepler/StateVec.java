package com.apophis.physics.kepler;

import com.apophis.maths.Vector3d;
import java.io.Serializable;

public class StateVec implements Serializable {

  public Vector3d r, v; // position and velocity
  public double x; // true anomaly
  
  public StateVec(Vector3d r, Vector3d v) {
    this.r = r;
    this.v = v;
  }
  
  @Override
  public String toString() {
    return "r="+r+" v="+v;
  }
}
