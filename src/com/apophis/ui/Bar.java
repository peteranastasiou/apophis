
package com.apophis.ui;

import com.apophis.gfx.graphic.Graphic;
import java.io.Serializable;

public class Bar implements Serializable {
  private double x, y, w, h;
  private double value;
  private Graphic barEnd, barMid; //consider using direct calls to mesh and shader and texture rather than graphics
  
  public Bar(double x, double y, double w, double h, double init) {
    barEnd = new Graphic();
    barEnd.setMesh("square");
    barEnd.setTexture("barLeft");
    barEnd.setShader("sprite");
    barEnd.setScale(1);
    
    barMid = new Graphic();
    barMid.setMesh("square");
    barMid.setTexture("barMid");
    barMid.setShader("sprite");
    barMid.setScale(1);
    
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    value = init;
  }
  
  public void setValue(double value) {
    this.value = value;
  }
  
  public void render() {
    //barEnd.render(viewer, Vector3d.zero, value);
  }
}
