
package com.apophis.ui.command;

import java.io.Serializable;

public interface Command extends Serializable {
    public int getKey();
    public void execute();
    public boolean isLive();
    public String getDescription();
}
