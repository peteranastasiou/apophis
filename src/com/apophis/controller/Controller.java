
package com.apophis.controller;

import com.apophis.Camera;
import com.apophis.entity.Entity;
import java.io.Serializable;

public interface Controller extends Serializable {
  public void control(Entity entity, long time);
  public void render(Entity e, Camera camera);
  //public void showUI();// TODO controller can inject UI elements
}
