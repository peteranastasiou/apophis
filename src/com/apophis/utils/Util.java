
package com.apophis.utils;

/**
 * @author peter.a
 */
public class Util {
  
  /**
   * Returns equivalent angle between 0 and 2*Pi
   * @param a
   * @return fixed angle
   */
  public static double fixAngle(double a) {
    while(a > 2*Math.PI) {
      a -= 2*Math.PI;
    }
    while(a < 0) {
      a += 2*Math.PI;
    }
    return a;
  }
  
  /**
   * Shortest path to get a to b. Including zero crossing.
   * Either returns [b - a], [b - a - 2*PI] or [b - a + 2*PI]
   * @param a from this angle
   * @param b to this angle
   * @return difference
   */
  public static double angleDist(double a, double b) {
    a = fixAngle(a);
    b = fixAngle(b);
    double way1 = b - a;
    double way2 = way1 + ((way1 > 0)?-2:2) * Math.PI;
    if(Math.abs(way1) < Math.abs(way2)) {
      return way1;
    } else {
      return way2;
    }
  }
  
  public static void delayms(long millis) {
    if(millis > 0) {
      try {
        Thread.sleep(millis);
      } catch (InterruptedException ex) {
        // Do Nothing
      }
    }
  }
}
