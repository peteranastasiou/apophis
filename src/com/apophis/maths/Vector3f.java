package com.apophis.maths;

import java.io.Serializable;

public class Vector3f implements Serializable {

  // Vector in graphic units and as floats.
  
  public float x, y, z;
  
  public Vector3f(Vector3d v) {
    x = (float)(v.x);
    y = (float)(v.y);
    z = (float)(v.z);
  }
  
  public Vector3f(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  public Vector3d toVector3d() {
    return new Vector3d(x, y, z);
  }
  
  @Override
  public String toString() {
    return x+","+y+","+z;
  }
  // do maths with Vector3d not here
}
