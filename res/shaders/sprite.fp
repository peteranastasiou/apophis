
#version 330 core

uniform sampler2D tex;

in vec2 vtc;
in float diffuse;

void main() {
    if(vtc.y < 0 || vtc.x < 0) {
        discard;
    }
    vec4 color = texture(tex, vtc);
    if (color.a < 0.01) {
        discard;
    }
    gl_FragColor = vec4(diffuse*color.rgb, color.a);
}
