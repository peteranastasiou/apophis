package com.apophis.ui;

import com.apophis.Const;
import com.apophis.Main;
import com.apophis.gfx.Color;
import com.apophis.gfx.Shader;
import com.apophis.gfx.graphic.Graphic;
import com.apophis.maths.Matrix4d;
import com.apophis.maths.Vector3d;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.Serializable;

public class Button implements Serializable {

  private static enum State {UP, DOWN, RELEASED}
  
  private final Rectangle box;
  private final Graphic graphic;
  private String msg;
  private State state;

  public Button(String msg, int x, int y, int w, int h) {
    this.msg = msg;
    box = new Rectangle(x, y, w, h);
    graphic = new Graphic();
    graphic.setMesh("rect");
    graphic.setShader("flat");
    graphic.setLayer(Const.LAYER_OVERLAY);
    state = State.UP;
  }

  public boolean wasClicked() {
    if(state == State.RELEASED) {
      state = State.UP;
      return true;
    }
    return false;
  }

  // Returns true if click was captured by this button
  public boolean invokeMouseDown(Point p) {
    System.out.println("Button test: "+p+" "+box);
    if(box.contains(p)) {
      state = State.DOWN;
      System.out.println("DOWN");
      return true;
    }
    return false;
  }

  // Returns true if click was captured by this button
  public void invokeMouseUp(Point p) {
    if(box.contains(p) && state == State.DOWN) {
      state = State.RELEASED;
      System.out.println("Released");
    } else {
      state = State.UP;
    }
  }

  public void render() {
    Shader shader = graphic.getShader();
    if(shader == null) return;

    Matrix4d pr = Matrix4d.orthographic(
        0.f, Main.WIDTH, Main.HEIGHT, 0.f, -100.0f, 100.0f);
    Matrix4d vw = Matrix4d.translate(new Vector3d(box.x, box.y, 100))
        .multiply(Matrix4d.scale(box.width, box.height, 1));

    shader.enable();
    shader.setUniform4f("color", (state == State.DOWN?Color.WHITE:Color.GRAY).to4f());
    shader.setUniformMat4f("pr_vw_md", pr.multiply(vw));
    graphic.getMesh().render();
    shader.disable();
  }
}
