
package com.apophis.controller;

import com.apophis.Main;
import com.apophis.Camera;
import com.apophis.entity.Entity;
import com.apophis.entity.Factory;
import com.apophis.gfx.graphic.PolyTrace;
import com.apophis.ui.Button;
import com.apophis.utils.Input;
import org.lwjgl.glfw.GLFW;

public class WalkerInputController implements Controller {
  private double walkVel = .3;
  private double jumpVel = .3;
  private long lastUpdated, downCounter;
  public int state; // -1, 0 or 1: left, still, right
  public Button clicker;
  
  public WalkerInputController() {
    clicker = new Button("Clicker", 10, 10, 150, 50);
    Main.getHUD().addButton(clicker);
  }
  
  @Override
  public void control(Entity entity, long time) {
    if(Main.universe().getFocused() != entity) {
      lastUpdated = time;
      return;
    }
    
    if(clicker.wasClicked()) {
      Factory.makeProjectile(Main.universe(), "rock", entity.getParentId(), 
        entity.getPosRel(), Main.universe().getTime());
    }
    
    PolyTrace trace = entity.getPhysics().getTrace();
    if(trace == null) {
      lastUpdated = time;
      return;
    }
    downCounter ++;
    if(Input.isKeyPressed(GLFW.GLFW_KEY_UP) && downCounter > 5) {
      entity.getPhysics().applyDeltaV(entity, jumpVel, trace.getPos().getAngle()-0.4*state-0.001, time);
      entity.getGraphic(0).setFrame(3);
      lastUpdated = time;
      downCounter = 0;
      return;
    }
    
    state = 0;
    if(Input.isKeyPressed(GLFW.GLFW_KEY_LEFT)) {
      entity.getGraphic(0).setFlip(true, false);
      state --;
    }
    if(Input.isKeyPressed(GLFW.GLFW_KEY_RIGHT)) {
      entity.getGraphic(0).setFlip(false, false);
      state ++;
    }
    if(state == 0) {
        entity.getGraphic(0).setFrame(0);
    } else {
      if(time % 10 < 5) {
        entity.getGraphic(0).setFrame(1);
      } else {
        entity.getGraphic(0).setFrame(2);
      }
    }
    
    // Update position:
    trace.walk(state*walkVel*(time - lastUpdated));
    // Ensure no angle offset:
    trace.resetAngleOffset();
    
    lastUpdated = time;
  }

  @Override
  public void render(Entity e, Camera c) {
  }
}
