package com.apophis;

import com.apophis.lib.Lib;
import com.apophis.entity.Entity;
import com.apophis.maths.Vector3d;
import com.apophis.utils.Util;
import java.io.Serializable;

public final class Camera implements Serializable {

  private Entity entity, oldEntity;
  private Vector3d pos = Vector3d.ZERO;
  private double transition;
  private double scale, oldScale;
  private double angle, oldAngle;

  public Camera(Entity entity, double scale) {
    setScale(scale);
    setEntity(entity);
  }

  public Entity getEntity() {
    return entity;
  }
  
  public double getScale() {
    return scale;
  }

  public double getAngle() {
    return angle;
  }

  public Vector3d getPos() {
    return pos;
  }

  public void setScale(double scale) {
    this.scale = scale;
  }

  public void switchToEntity(Entity entity) {
    if(this.entity != null) {
      oldEntity = this.entity;
      transition = 0;
      oldScale = scale;
      oldAngle = angle;
    }
    this.entity = entity;
  }
  
  public void setEntity(Entity entity) {
    oldEntity = null;
    this.entity = entity;
  }
  
  public void update() {
    if(entity == null) {
      pos = Vector3d.ZERO;
    } else {
      // Update Position (springy):
      Vector3d newPos = entity.getPosAbs();
      if(oldEntity == null) {
        pos = newPos;
        angle = (Math.PI/2.-entity.getViewAngle());
      } else {
        
        
        transition = 0.1 + 0.9 * transition;
        
        //Vector3d oldPos = oldEntity.getPosAbs();
        //Vector3d travel = newPos.subtract(oldPos);
        //pos = oldPos.add(travel.normalise().scale(transition * travel.length()));
        pos = newPos;
        
        if(transition < 0.99) {
          double newScale = entity.getViewDistance()/20;
          scale = oldScale + transition*(newScale - oldScale);
          if(entity.getParent() != null) {
            double newAngle = Util.fixAngle((Math.PI/2.-entity.getViewAngle()));
            angle = oldAngle + transition*(newAngle - oldAngle);
          }
        }
      }
      
      // Update Star Shader:
      float starscale= 1E8f;
      Lib.getShader("starnest").setUniform2f("shift",
        (float)(pos.x / starscale), (float)(pos.y / starscale));
      Lib.getShader("starnest").setUniform1f("scale", 0.1f*(float)Math.pow(scale, 0.1));
      Lib.getShader("starnest").setUniform1f("angle", (float)angle);
    }
  }
}
