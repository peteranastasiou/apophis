package com.apophis.gfx.graphic;

import com.apophis.gfx.Color;
import com.apophis.gfx.ColorScheme;
import com.apophis.gfx.Mesh;
import com.apophis.maths.Vector3d;
import com.apophis.utils.SimplexNoise;

public class Sphere {

  public static Mesh newHemisphere(Vector3d pos, ColorScheme s) {
    return newSphere(pos, 0.5f, 64, s);
  }

  public static Mesh newSphere(Vector3d pos, ColorScheme s) {
    return newSphere(pos, 1.0f, 64, s);
  }

  public static Mesh newSphere(Vector3d pos, float segment, int divs, ColorScheme scheme) {
    // Number of segments:
    int vertDiv = divs;
    int horzDiv = (int)(2 * divs * segment);

    // create arrays:
    float[] vertices = new float[3 * vertDiv * horzDiv];
    float[] normals = new float[3 * vertDiv * horzDiv];
    int[] indices = new int[6 * vertDiv * horzDiv];
    float[] colors = new float[3 * vertDiv * horzDiv];

    float vf = 1.0f / (float)(vertDiv - 1);
    float hf = segment / (float)(horzDiv - 1); // set 0.5 to 1 for full sphere
    int vi, hi;

    int vertexCount = 0;
    int normalCount = 0;
    int colorCount = 0;
    int indexCount = 0;

    // Construct vertices of sphere radius 1.0:

    double oct1 = .9;   // smoothness of first noise octave
    double oct2 = 0.5; // smoothness of second noise octave

    for(vi = 0; vi < vertDiv; vi++) {
      for(hi = 0; hi < horzDiv; hi++) {
        // Unit sphere coords (must be unit for normals):
        float x = (float)(Math.cos(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        float y = (float)(Math.sin(-Math.PI / 2 + Math.PI * vi * vf));
        float z = (float)(Math.sin(2 * Math.PI * hi * hf) * Math.sin(Math.PI * vi * vf));
        vertices[vertexCount++] = x;
        vertices[vertexCount++] = y;
        vertices[vertexCount++] = z;
        
        // In this case the normals are the vertices! (Already Normalised)
        normals[normalCount++] = x;
        normals[normalCount++] = y;
        normals[normalCount++] = z;

        // coloring with simplex noise:
        float no1 = 0.5f + (float)SimplexNoise.noise(pos.x + x / oct1, pos.y + y / oct1, z / oct1);
        float no2 = 0.5f + (float)SimplexNoise.noise(pos.x + x / oct2, pos.y + y / oct2, z / oct2);
        float noise = 0.9f * no1 + 0.2f * no2;

        Color c = scheme.getColor(noise);
        colors[colorCount++] = c.r;
        colors[colorCount++] = c.g;
        colors[colorCount++] = c.b;
      }
    }

    // Construct indices
    // Start with special case of bottom sub-division where all vertices meet
    vi = 0;
    for(hi = 0; hi < horzDiv - 1; hi++) {
      indices[indexCount++] = vi * horzDiv + hi;
      indices[indexCount++] = (vi + 1) * horzDiv + hi;
      indices[indexCount++] = (vi + 1) * horzDiv + (hi + 1);
    }

    // Middle divisions
    for(vi = 1; vi < vertDiv - 2; vi++) {
      for(hi = 0; hi < horzDiv - 1; hi++) {
        indices[indexCount++] = vi * horzDiv + hi;
        indices[indexCount++] = (vi + 1) * horzDiv + (hi + 1);
        indices[indexCount++] = vi * horzDiv + (hi + 1);
        indices[indexCount++] = vi * horzDiv + hi;
        indices[indexCount++] = (vi + 1) * horzDiv + hi;
        indices[indexCount++] = (vi + 1) * horzDiv + (hi + 1);
      }
    }

    // Cap off the top of the sphere
    vi = vertDiv - 2;
    for(hi = 0; hi < horzDiv - 1; hi++) {
      indices[indexCount++] = vi * horzDiv + hi;
      indices[indexCount++] = (vi + 1) * horzDiv + (hi + 1);
      indices[indexCount++] = vi * horzDiv + (hi + 1);
    }
    Mesh m = new Mesh(indices.length);
    m.loadVertexBuffer(vertices);
    m.loadColorBuffer(colors);
    m.loadNormalBuffer(normals);
    m.loadIndexBuffer(indices);
    
    return m;
  }
}
