package com.apophis.gfx;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import com.apophis.utils.BufferUtils;

// Indexed OpenGL Vertex Array Object
public class Mesh {

  private final int vao; // vertex array object
  private int ibo, vbo, cbo, nbo, tbo; // vertex buffer objects
  private int count;
  private int mode = GL_TRIANGLES;
  private boolean dynamicDraw;
  
  public Mesh(int count) {
    vao = glGenVertexArrays();
    this.count = count;
  }
  
  public Mesh(int count, boolean dynamicDraw) {
    vao = glGenVertexArrays();
    this.count = count;
    this.dynamicDraw = dynamicDraw;
  }
  
  public void setCount(int count) {
    this.count = count;
  }
  
  // STATIC LOADING:
  public void loadVertexBuffer(float[] vertices) {
    vbo = loadFloatBuffer(vertices, 3, Shader.VERTEX_ATTRIB);
  }
  
  public void loadTexCoordBuffer(float[] texCoords) {
    tbo = loadFloatBuffer(texCoords, 2, Shader.TCOORD_ATTRIB);
  }
  
  public void loadNormalBuffer(float[] normals) {
    nbo = loadFloatBuffer(normals, 3, Shader.NORMAL_ATTRIB);
  }
  
  public void loadColorBuffer(float[] colors) {
    cbo = loadFloatBuffer(colors, 3, Shader.COLOR_ATTRIB);
  }
  
  private int loadFloatBuffer(float[] data, int datasize, int attrib) {
    glBindVertexArray(vao);
    int buf = glGenBuffers();
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, BufferUtils.createFloatBuffer(data),
      dynamicDraw?GL_DYNAMIC_DRAW:GL_STATIC_DRAW);
    glVertexAttribPointer(attrib, datasize, GL_FLOAT, false, 0, 0);
    glEnableVertexAttribArray(attrib);
    return buf;
  }
  
  public void loadIndexBuffer(int[] data) {
    glBindVertexArray(vao);
    ibo = glGenBuffers();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, BufferUtils.createIntBuffer(data), 
      dynamicDraw?GL_DYNAMIC_DRAW:GL_STATIC_DRAW);
    
    // unbind:
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  
  // DYNAMIC RELOADING:
  public void reloadVertexBuffer(float[] vertices) {
    reloadFloatBuffer(vbo, vertices);
  }
  
  public void reloadTexCoordBuffer(float[] texCoords) {
    reloadFloatBuffer(tbo, texCoords);
  }
  
  public void reloadNormalBuffer(float[] normals) {
    reloadFloatBuffer(nbo, normals);
  }
  
  public void reloadColorBuffer(float[] colors) {
    reloadFloatBuffer(cbo, colors);
  }
  
  private void reloadFloatBuffer(int buf, float[] data) {
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    
    // NOTE BufferSubData sometimes doesn't work!! Could consider glMapBuffer...
    //glBufferSubData(GL_ARRAY_BUFFER, 0, BufferUtils.createFloatBuffer(data));
    
    // BufferData will re-allocate array and delete old:
    glBufferData(GL_ARRAY_BUFFER, BufferUtils.createFloatBuffer(data), GL_DYNAMIC_DRAW);
  }
  
  public void setMode(int mode) {
    this.mode = mode;
  }
  
  public void bind() {
    glBindVertexArray(vao);
    if(ibo > 0) {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    }
  }

  public void unbind() {
    if(ibo > 0) {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    glBindVertexArray(0);
  }

  public void draw() {
    if(ibo > 0) {
      glDrawElements(mode, count, GL_UNSIGNED_INT, 0);
    } else {
      glDrawArrays(mode, 0, count);
    }
  }

  public void render() {
    bind();
    draw();
  }

}
