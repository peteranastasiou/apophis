
package com.apophis.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Template implements Serializable {

  public static enum ClassType {
    CRAFT, MOB, PLANET, PROJECTILE
  }
  
  private final String name;
  private final ClassType classType;
  private final Map<String, Object> map;
  
  public Template(String name, ClassType classType) {
    this.name = name;
    this.classType = classType;
    map = new HashMap<>();
  }
  
  public String getName() {
    return name;
  }
  
  public void put(String key, Object obj) {
    map.put(key, obj);
  }
  
  public Object get(String key) {
    Object obj = map.get(key);
    if(obj == null) {
      System.err.println("Template " + name + " has no field: " + key);
    }
    return obj;
  }
  
  public ClassType getClassType() {
    return classType;
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(name);
    sb.append(" {\n");
    for(Map.Entry<String, Object> entry : map.entrySet()) {
        sb.append("  ")
          .append(entry.getKey())
          .append(" = ")
          .append(entry.getValue())
          .append("\n");
    }
    sb.append("}");
    return sb.toString();
  }
}
