
package com.apophis.controller;

import com.apophis.ui.HUD;
import com.apophis.Main;
import com.apophis.Camera;
import com.apophis.ui.command.Command;
import com.apophis.ui.command.EntryCommand;
import com.apophis.ui.command.ExitCommand;
import com.apophis.entity.Entity;
import com.apophis.physics.Physics;
import com.apophis.utils.Input;
import com.apophis.utils.Util;
import org.lwjgl.glfw.GLFW;

public class CraftInputController implements Controller {
  private boolean init = false;
  private int hull, shield; // between 0 and max
  private double throttleLimit; // between 0 and 1
  private double throttle; // between 0 and 1
  private double massFuel, massInit; // kg
  private double velExhaust; // m/s
  private double massFlow; // delta kg
  private double angleSetPt, angleVel; // Smooth Angle Motion
  private Command enterCmd, exitCmd;
  
  public CraftInputController() {
    hull = 10;
    shield = 6;
    massFuel = 400;
    massInit = 100;
    velExhaust = 20;
    throttleLimit = .5;
    massFlow = 1;
  }
  
  @Override
  public void control(Entity entity, long time) {
    // Initialise:
    if(init == false) {
      angleSetPt = entity.getAngle();
      entity.getGraphic(1).setScale(0);
      entity.getGraphic(2).setScale(0);
      init = true;
    }
    
    // Manage Entry/Exit Commands:
    Entity focused = Main.universe().getFocused();
    if(focused != entity) {
      if(entity.getOutline() == null) {
        System.err.println("Entity has no outline: "+entity.toString());
      }
      double reach = entity.getOutline().getScale();
      double dist = entity.getPosRel().distanceTo(focused.getPosRel());
      if(dist < reach) {
        if(enterCmd == null) {
          enterCmd = new EntryCommand(GLFW.GLFW_KEY_F, focused, entity,
                                     "Enter Craft");
          exitCmd = null;
          Main.getHUD().addCommand(enterCmd);
        }
      } else {
        if(enterCmd != null) {
          Main.getHUD().removeCommand(enterCmd);
          enterCmd = null;
        }
      }
      return;
    }
    
    // Exit Craft:
    if(exitCmd == null) {
      exitCmd = new ExitCommand(GLFW.GLFW_KEY_G, entity, "Exit Craft");
      enterCmd = null;
      Main.getHUD().addCommand(exitCmd);
    }
    
    // Apply Throttle Limit Control:
    if(Input.isKeyPressed(GLFW.GLFW_KEY_LEFT_SHIFT)) {
      throttleLimit += 0.006;
      if(throttleLimit > 1) {
        throttleLimit = 1;
      }
    } else if (Input.isKeyPressed(GLFW.GLFW_KEY_LEFT_CONTROL)) {
      throttleLimit -= 0.006;
      if(throttleLimit < 0) {
        throttleLimit = 0;
      }
    }
    
    // Apply Throttle On/Off Control:
    Physics physics = entity.getPhysics();
    if(Input.isKeyPressed(GLFW.GLFW_KEY_UP)) {
      throttle += 0.1;
      if(throttle >= throttleLimit) throttle = throttleLimit;
    } else if(Input.isKeyPressed(GLFW.GLFW_KEY_DOWN)) {
      throttle -= 0.1;
      if(throttle >= -throttleLimit) throttle = -throttleLimit;
    } else {
      throttle /= 2;
      if(throttle <= 0.001) throttle = 0;
    }
    
    if(throttle > 0 && massFuel > 0) {
      Main.universe().resetTimeSpeed();
      // The rocket equation:
      double dMass = throttle*massFlow;
      if(massFuel < dMass) dMass = massFuel;
      massFuel -= dMass;
      double mass = massFuel + massInit;
      double dv = velExhaust*dMass/mass;
      physics.applyDeltaV(entity, dv, entity.getAngle(), time);
      // Update jet graphic:
      entity.getGraphic(1).setScale(throttle*10, throttle*1.3);
      entity.getGraphic(2).setScale(throttle*10, throttle*1.3);
    } else {
      // Update jet graphic:
      entity.getGraphic(1).setScale(0);
      entity.getGraphic(2).setScale(0);
    }
    
    double r = entity.getPosRel().length();
    double a = entity.getParent().getPlanet().r_atm;
    double s = entity.getParent().getPlanet().r_sea;
    
    // Atmospheric Resistance:
    if(r < a && !physics.isStatic()) {
      // in atmosphere to degree:
      double level = 1 - (r - s) / (a - s);
      level *= level; // deminishes by squared law
      double v = entity.getVelRel().length();
      double airRes = -0.006*level*v*v; // air resistance is proportional to v^2
      physics.applyDeltaV(entity, airRes, entity.getVelRel().getAngle(), time);
    }
    
    // Do Angular Physics (P-controller):
    double angle  = Util.fixAngle(entity.getAngle());
    if(physics.isStatic()) {
      angleSetPt = angle;
    } else {
      if(Input.isKeyPressed(GLFW.GLFW_KEY_LEFT)) {
        Main.universe().resetTimeSpeed();
        angleSetPt += 0.03f;
      }
      if(Input.isKeyPressed(GLFW.GLFW_KEY_RIGHT)) {
        Main.universe().resetTimeSpeed();
        angleSetPt -= 0.03f;
      }
      double cmd = 0.15*Util.angleDist(angle, angleSetPt);
      entity.setAngle(angle + cmd);
    }
  }
  
  @Override
  public void render(Entity e, Camera c) {
    HUD hud = Main.getHUD();
    hud.printL("");
    hud.printL("fuel:   "+(int)massFuel);
    hud.printL("limit:  "+(int)(throttleLimit*100)+"%");
    hud.printL("thrust: "+(int)(throttle*100)+"%");
    hud.printL("");
    hud.printL("shield: "+shield);
    hud.printL("hull:   "+hull);
  }
}
