package com.apophis.gfx.graphic;

import com.apophis.Const;
import com.apophis.maths.Matrix4d;
import com.apophis.lib.Lib;
import com.apophis.Main;
import com.apophis.lib.Res;
import com.apophis.Camera;
import com.apophis.gfx.Mesh;
import com.apophis.gfx.Shader;
import com.apophis.gfx.Texture;
import com.apophis.maths.Vector3d;
import java.io.Serializable;

public final class Graphic implements Serializable {

  private Res<Mesh> mesh;
  private String shaderName;
  private Res<Texture> texture;
  private Vector3d offset = new Vector3d(0, 0, 0);
  private int layer = 0;
  private int frame = 0;
  private int numFrames = 1;
  private Vector3d unormal = Vector3d.ZERO;
  private Vector3d material = new Vector3d(0.05, 1, 0.); // ambient, diffuse, specular
  private float scalex = 1f;
  private float scaley = 1f;
  private float xFlip = 1f;
  private float yFlip = 1f;

  public Graphic() {
  }

  public void setLayer(int layer) {
    this.layer = layer;
  }

  public int getLayer() {
    return layer;
  }

  public void setNumFrames(int frames) {
    if(frames > 0) {
      numFrames = frames;
    }
  }

  public int getNumFrames() {
    return numFrames;
  }

  public void setFrame(int frame) {
    this.frame = frame;
  }

  public int getFrame() {
    return frame;
  }

  // set Mesh to a shared library resource
  public void setMesh(String meshKey) {
    this.mesh = new Res<>(Lib.MESH, meshKey);
  }
  
  // set Mesh to a unique (e.g. generated) mesh
  public void setMesh(Mesh mesh) {
    this.mesh = new Res<>(mesh);
  }

  public Mesh getMesh() {
    if(mesh == null) return null;
    return mesh.get();
  }

  public void setShader(String shaderName) {
    this.shaderName = shaderName;
  }

  public Shader getShader() {
    return Lib.getShader(shaderName);
  }

  public void setTexture(Texture texture) {
    this.texture = new Res<>(texture);
  }
  
  public void setTexture(String texture) {
    this.texture = new Res<>(Lib.TEXTURE, texture);
  }

  public Texture getTexture() {
    if(texture == null) return null;
    return texture.get();
  }

  public void setScale(double a) {
    scalex = (float)a;
    scaley = (float)a;
  }

  public void setScale(double x, double y) {
    scalex = (float)x;
    scaley = (float)y;
  }

  public float getScale() {
    return scaley;
  }

  public void setMode(int md) {
    if(mesh != null) {
      getMesh().setMode(md);
    }
  }

  public void setOffset(Vector3d pos) {
    offset = pos;
  }
  
  // Only used for sprites which have only one (uniform) lighting normal
  public void setLightingNormal(Vector3d normal) {
    unormal = normal;
  }
  
  // Lighting material is levels of ambient, diffuse, specular: 0 to 1 in xyz
  public void setMaterial(Vector3d material) {
    this.material = material;
  }

  public void setFlip(boolean xFlip, boolean yFlip) {
    this.xFlip = xFlip?-1f:1f;
    this.yFlip = yFlip?-1f:1f;
  }

  /**
   * render this graphic at position [pos], with angle [angle]. the camera gives
   * the current position and angular offset.
   *
   * @param camera camera position and angle
   * @param mPos model position
   * @param mAngle model angle
   */
  public void render(Camera camera, Vector3d mPos, double mAngle) {
    Shader shader = Lib.getShader(this.shaderName);
    Matrix4d md;

    Matrix4d vw;
    Matrix4d pr;
    float ratio = (float)Main.HEIGHT / (float)Main.WIDTH; // TODO support re-sizable window

    if(camera == null) { // Used for HUD drawing
      // The view centered on the screen:
      vw = Matrix4d.translate(mPos.add(new Vector3d(0, 0, layer)));
      md = Matrix4d.identity();
      pr = Matrix4d.orthographic(
        -1.0f, 1.0f, -1.0f * ratio, 1.0f * ratio, -100.0f, 100.0f);

    } else {
      // Camera position:
      Vector3d cPosf = camera.getPos().scale(-1.0);
      // Model position:
      Vector3d mPosf = mPos.add(new Vector3d(0.0, 0.0, layer));
      
      // View Matrix: Locate camera
      vw = Matrix4d.rotateZ(camera.getAngle())
        .multiply(Matrix4d.scale(Const.GRAPHIC_SCALE))
        .multiply(Matrix4d.translate(cPosf));
      
      // Model Matrix: Locate model in the world
      // (note matrix multiplications are in reverse order of operations)
      md = Matrix4d.translate(mPosf)
        .multiply(Matrix4d.rotateZ((float)mAngle))
        .multiply(Matrix4d.translate(offset))
        .multiply(Matrix4d.scale(xFlip * scalex, yFlip * scaley, scaley));

      // Perspective Matrix: Transform 3d to 2d
      float vscale = (float)camera.getScale();
      pr = Matrix4d.orthographic(
        -vscale, vscale, -vscale * ratio, vscale * ratio, -100.0f, 100.0f);
    }

    // TODO compare efficiency of multiplying pr and vw here or in the shader?
    shader.enable();
    shader.setUniformMat4f("pr_vw_md", pr.multiply(vw).multiply(md));
    if(shader.hasUniform("md"))  shader.setUniformMat4f("md", md);
    if(shader.hasUniform("material"))  shader.setUniform3f("material", material.toVector3f());
    if(shader.hasUniform("frame")) shader.setUniform2f("frame", ((float)frame) / numFrames, 1f / numFrames);
    if(shader.hasUniform("unormal")) shader.setUniform3f("unormal", unormal.toVector3f());
    if(getTexture() != null) getTexture().bind();
    if(getMesh() != null) getMesh().render();
    shader.disable();
  }
}
