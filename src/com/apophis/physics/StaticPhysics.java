
package com.apophis.physics;

import com.apophis.Camera;
import com.apophis.entity.Entity;
import com.apophis.gfx.graphic.PolyTrace;

public class StaticPhysics implements Physics {

  @Override
  public final void init(Entity entity, long time) {
    
  }
  
  @Override
  public void update(Entity e, long time) {
  }

  @Override
  public void project(Entity e, long time) {
  }

  @Override
  public void applyDeltaV(Entity e, double dv, double angle, long time) {
  }

  @Override
  public boolean isStatic() {
    return true;
  }

  @Override
  public PolyTrace getTrace() {
    return null;
  }
  
  @Override
  public void render(Entity e, Camera c) {
  }
}
