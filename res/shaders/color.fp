#version 330 core

varying vec4 vColor; // interpolated Color

void main() {
    gl_FragColor = vColor;
}

