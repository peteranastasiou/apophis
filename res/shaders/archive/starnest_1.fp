
// Star Nest by Pablo Román Andrioli
// https://www.shadertoy.com/view/XlfGRj
// This content is under the MIT License.

uniform float scale;
uniform vec2 resolution;
uniform vec2 shift;

#define iterations 17
#define formuparam 0.53

#define volsteps 20
#define stepsize 0.1

#define zoom   1.000
#define tile   0.9  // 0.0 looks trippy bubble universe
#define speed  0.010 

#define brightness 0.001
#define darkmatter 0.4
#define distfading 0.830
#define saturation 0.800

void main(void) {
	//get coords and direction
	vec2 uv=scale*(gl_FragCoord.xy-resolution.xy/2.)/resolution.xy;
	uv.y*=resolution.y/resolution.x;
	vec3 dir=vec3(uv*zoom,1.);

	vec3 from=vec3(1.,.5,0.5);
	from+=vec3(shift.x, shift.y, -2.0);

	//volumetric rendering
	float s=0.1,fade=1.;
	vec3 v=vec3(0.);
	for (int r=0; r<volsteps; r++) {
		vec3 p=from+s*dir*.5;
		p = abs(vec3(tile)-mod(p,vec3(tile*2.))); // tiling fold
		float pa,a=pa=0.;
		for (int i=0; i<iterations; i++) { 
			p=abs(p)/dot(p,p)-formuparam; // the magic formula
			a+=abs(length(p)-pa); // absolute sum of average change
			pa=length(p);
		}
		float dm=max(0.,darkmatter-a*a*.001); //dark matter
		a*=a*a; // add contrast
		if (r>2) fade*=1.-dm; // dark matter, don't render near
		//v+=vec3(dm,dm*.5,0.);
		v+=fade;
		v+=vec3(s,s*s,s*s*s*s)*a*brightness*fade; // coloring based on distance
		fade*=distfading; // distance fading
		s+=stepsize;
	}
	v=mix(vec3(length(v)),v,saturation); //color adjust
	gl_FragColor = vec4(v*.01,1.);
	
}