package com.apophis.gfx;

import java.io.Serializable;

public class Color implements Serializable {

  public float r, g, b, a;
  
  public static final Color WHITE = new Color(1f,1f,1f);
  public static final Color GRAY = new Color(.3f,.3f,.3f,1f);
  public static final Color BLACK = new Color(0f,0f,0f);
  public static final Color RED = new Color(1f,0f,0f);
  public static final Color CYAN = new Color(0f,1f,1f);
  public static final Color BLUE = new Color(0f,0f,1f);
  public static final Color MAGENTA = new Color(1f,0f,1f);
  public static final Color YELLOW = new Color(1f,1f,0f);
  public static final Color GREEN = new Color(0f,1f,0f);
  
  public Color(float r, float g, float b) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = 1.0f;
  }

  public Color(float r, float g, float b, float a) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  
  public static Color blend(float x1, float x2, float x, Color c1, Color c2) {
    float ratio = (x - x1) / (x2 - x1);
    float r = (1 - ratio) * c1.r + ratio * c2.r;
    float g = (1 - ratio) * c1.g + ratio * c2.g;
    float b = (1 - ratio) * c1.b + ratio * c2.b;
    float a = (1 - ratio) * c1.a + ratio * c2.a;
    return new Color(r, g, b, a);
  }

  public Color copy() {
    return new Color(r, g, b, a);
  }
  
  public float[] to4f() {
    return new float[]{r, g, b, a};
  }
}
