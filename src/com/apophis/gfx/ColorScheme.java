package com.apophis.gfx;

// TODO To a Lib type!

public class ColorScheme {

  private final Color c0, c1, c2, c3;

  public ColorScheme(Color c0, Color c1, Color c2, Color c3) {
    this.c0 = c0;
    this.c1 = c1;
    this.c2 = c2;
    this.c3 = c3;
  }

  public Color getColor(float level) {
    if(level < 0.33f) {
      return Color.blend(0.0f, 0.33f, level, c0, c1);
    } else if(level < 0.66f) {
      return Color.blend(0.33f, 0.66f, level, c1, c2);
    } else {
      return Color.blend(0.66f, 1.0f, level, c2, c3);
    }
  }

  public final static ColorScheme blueWater = new ColorScheme(
    new Color(0.0470588f, 0.1921569f, 0.2980392f),
    new Color(0.0470588f, 0.3450980f, 0.4078431f),
    new Color(0.1294117f, 0.5137254f, 0.6901961f),
    new Color(0.1686274f, 0.7176471f, 0.8156863f));

  public final static ColorScheme greenLand = new ColorScheme(
    new Color(0.0f, 0.0f, 0.0f),
    new Color(0.0f, 0.5f, 0.5f),
    new Color(0.4f, 0.8f, 0.0f),
    new Color(0.9f, 0.9f, 0.9f));

  public final static ColorScheme aridLand = new ColorScheme(
    new Color(0.7070f, 0.4414f, 0.2891f),
    new Color(0.8438f, 0.4570f, 0.2422f),
    new Color(0.9961f, 0.6016f, 0.2969f),
    new Color(0.9882f, 0.7813f, 0.6250f));

  public final static ColorScheme sun = new ColorScheme(
    new Color(0.0000f, 0.0000f, 0.0000f),
    new Color(1.0000f, 0.6855f, 0.0000f),
    new Color(1.0000f, 0.9900f, 0.0000f),
    new Color(1.0000f, 1.0000f, 1.0000f));
  
  public final static ColorScheme hotLand = new ColorScheme(
    new Color(0.0000f, 0.0000f, 0.0000f),
    new Color(1.0000f, 0.6855f, 0.0000f),
    new Color(1.0000f, 0.0240f, 0.0000f),
    new Color(1.0000f, 1.0000f, 1.0000f));

  public final static ColorScheme grayLand = new ColorScheme(
    new Color(0.23f, 0.2f, 0.2f),
    new Color(0.32f, 0.3f, 0.3f),
    new Color(0.45f, 0.4f, 0.4f),
    new Color(0.62f, 0.6f, 0.6f));
  
  public final static ColorScheme gasLand = new ColorScheme(
    new Color(250/255f, 101/255f, 498/255f),
    new Color(249/255f, 149/255f,  87/255f),
    new Color(244/255f, 155/255f, 123/255f),
    new Color(226/255f, 186/255f, 193/255f));
}
