
package com.apophis.utils;;

import java.util.Random;

public class Rando {

	private static long seed = 0;
	private static final Random random = new Random(0);

	public static void setSeed(long seed) {
		Rando.seed = seed;
		random.setSeed(seed);
	}

	public static long getSeed() {
		return seed;
	}

  /**
   * @param from
   * @param to
   * @return random double between from and to
   */
	public static double randDouble(double from, double to) {
		double r = random.nextDouble();
		return from + r*(to - from);
	}
  
  /**
   * @param from
   * @param to
   * @return random integer between from (inclusive) and to (inclusive)
   */
	public static int randInt(int from, int to) {
		double r = random.nextDouble();
		return (int)Math.min(to, Math.floor(from + r*(1 + to - from)));
	}
  
  /**
   * Returns true 1 in n times where n = rate
   * @param rate
   * @return boolean result
   */
  public static boolean oneIn(int rate) {
    int i = random.nextInt();
    return i % rate == 0;
  }
}
