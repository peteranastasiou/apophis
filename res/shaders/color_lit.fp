
#version 330 core

uniform vec3 material; //x = ambient, y = diffuse, z = specular

in vec4 vColor;
in vec3 vNormal;
in vec3 vLight;

void main() {
    // Ambient:
    float ambient = material.x;
    // Diffuse:
    float diffuse = material.y * dot(vLight, vNormal);
    if (diffuse < 0.) diffuse = 0.;
    // Specular:
    vec3 v = vec3(0, 0, -1.); // viewing direction
    vec3 r = reflect(vLight, vNormal); // reflected light vector
    float specular = dot(v, r);
    specular = material.z * clamp(pow(specular, 9), 0, 1);// shininess of 9
    // Apply color:
    gl_FragColor = vec4((diffuse + ambient) * vColor.rgb + specular * vec3(1), vColor.a);
}

