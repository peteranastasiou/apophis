
package com.apophis.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Log {
  
  private static PrintWriter writer = null;
  
  public static boolean init(File file) {
    try {
      FileWriter fwriter = new FileWriter(file);
      writer = new PrintWriter(fwriter);
    } catch(IOException ex) {
      return false;
    }
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        close();
      }
    });
    return true;
  }
  
  public static PrintWriter log() {
    return writer;
  }
  
  private static void close() {
    if(writer != null) {
      writer.close();
      writer = null;
    }
  }
}
