package com.apophis.maths;

import java.io.Serializable;

public class Vector3d implements Serializable {

  public double x, y, z;
  public static final Vector3d ZERO = new Vector3d(0);
  
  public Vector3d(double v) {
    this.x = v;
    this.y = v;
    this.z = v;
  }

  public Vector3d(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  // init from polar coordinates:
  public Vector3d(double r, double a) {
    x = r * Math.cos(a);
    y = r * Math.sin(a);
    z = 0;
  }

  public Vector3d scale(double s) {
    return new Vector3d(x * s, y * s, z * s);
  }

  public Vector3d add(Vector3d v) {
    return new Vector3d(x + v.x, y + v.y, z + v.z);
  }

  public Vector3d subtract(Vector3d v) {
    return new Vector3d(x - v.x, y - v.y, z - v.z);
  }
  
  public double distanceTo(Vector3d v) {
    return subtract(v).length();
  }
  
  public Vector3d subtractXY(Vector3d v) {
    double x1 = x - v.x;
    double y1 = y - v.y;
    return new Vector3d(x1, y1, z);
  }

  public double dot(Vector3d v) {
    return x * v.x + y * v.y + z * v.z;
  }

  public Vector3d cross(Vector3d v) {
    double x0 = y * v.z - z * v.y;
    double y0 = z * v.x - x * v.z;
    double z0 = x * v.y - y * v.x;
    return new Vector3d(x0, y0, z0);
  }

  // Special multiplication used for 2D intersection calculation:
  public double multItx(Vector3d v) {
    return x*v.y - y*v.x;
  }
  
  // length aka norm
  public double length() {
    return Math.sqrt(x * x + y * y + z * z);
  }

  public Vector3d normalise() {
    double len = length();
    return new Vector3d(x/len, y/len, z/len);
  }
  
  public double getAngle() {
    return Math.atan2(y, x);
  }
  
  /**
   * This gives the angle difference between the vectors as if they were both
   * emanating from the same point. The angle is positive if the argument vector
   * is counter clockwise from this vector and negative if clockwise.
   * @param v the argument vector
   * @return angle between vectors
   */
  public double angleBetween(Vector3d v) {
    // absolute value given by cosine rule:
    //TODO SURPRISINGLY SLOW CALL TO ACOS (20% of all game time when profiled
    double angle = Math.acos(dot(v) / length() / v.length());
    // z component of cross product gives us the winding CW/CCW:
    if(x * v.y > y * v.x) {
     return angle;
    } else {
      return -angle;
    }
  }
  
  public double angleTo(Vector3d v) {
    return Math.atan2(v.y - y, v.x - x);
  }
  
  public Vector3f toVector3f() {
    return new Vector3f(this);
  }
  
  @Override
  public String toString() {
    return String.format("[%1.02f, %1.02f, %1.02f]", x, y, z);
  }
}
