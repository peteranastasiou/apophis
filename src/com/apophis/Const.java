package com.apophis;

public class Const {
  public static final double G = 1E-20; //6.67408E-20; // km^3kg^-1s^-2
	public static final double AU = 149E9; // m
	public static final long SEC_PER_YEAR = 31536000;
	public static final long SEC_PER_DAY = 86400;
	public static final long SEC_PER_HOUR = 3600;
	public static final long SEC_PER_MINUTE = 60;
  public static final double VIEW_SCALE_ORBIT = .004;
  public static final float GRAPHIC_SCALE = 1E-4f; // TODO changing messes up symbols - please fix
  
  public static final int LAYER_OVERLAY = 1000;
  public static final int LAYER_PLANET = 800;
  public static final int LAYER_MOB = 6;
  public static final int LAYER_CRAFT = 4;
  public static final int LAYER_FEATURE = -96;
  public static final int LAYER_ATMOSPHERE = -98;
  public static final int LAYER_UNDERLAY = -97;
  public static final int LAYER_BACKGROUND = -99;
  
  public static boolean DRAW_OUTLINES = false;
}
