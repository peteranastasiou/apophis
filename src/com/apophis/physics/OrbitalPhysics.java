package com.apophis.physics;

import com.apophis.Const;
import com.apophis.Main;
import com.apophis.Camera;
import com.apophis.entity.Entity;
import com.apophis.entity.Planet;
import com.apophis.gfx.Color;
import com.apophis.gfx.graphic.PolyTrace;
import com.apophis.gfx.graphic.Polyline;
import com.apophis.maths.Line3d;
import com.apophis.maths.Line3d.CollisionResult;
import com.apophis.maths.Vector3d;
import com.apophis.physics.kepler.Kepler;
import com.apophis.physics.kepler.OrbitalElements;
import com.apophis.physics.kepler.StateVec;

public class OrbitalPhysics implements Physics {

  // State:
  // dynamic means that this orbit may change
  // parked means the entity is temporarily fixed relative to parent
  private boolean isDynamic;
  private boolean isParked;
  
  // Orbit Data:
  private long refTime;
  private StateVec refState;
  private Vector3d orbitEndPoint;
  private OrbitalElements elements;
  
  // Parked Data:
  private PolyTrace trace;
  
  // Graphic:
  private final Polyline path, vectorPl;
  private double viewOrbitCutoff; // Deprecated

  public OrbitalPhysics(Entity entity, PolyTrace trace, 
                        boolean isDynamic, long time) {
    // Set Data:
    this.isParked = true;
    this.isDynamic = isDynamic;
    this.trace = trace;
    path = new Polyline(true);
    vectorPl = new Polyline(true);
    
    // Set relative position to parent:
    double offset = entity.getGraphic(0).getScale();
    entity.setPosRel(trace.getPos().add(new Vector3d(offset, trace.getPos().getAngle())));
    // Set relative velocity to parent:
    entity.setVelRel(Vector3d.ZERO);
  }
  
  public OrbitalPhysics(Entity entity, Vector3d posAbs, Vector3d velAbs, 
                        boolean isDynamic, long time) {
    // Set Data:
    this.isParked = false;
    this.isDynamic = isDynamic;
    path = new Polyline(true);
    vectorPl = new Polyline(true);
    
    // Find influencing body: parent
    Entity parent = Main.universe().getInfluence(posAbs);
    
    // Initialise Orbit:
    if(parent != null) {
      entity.setParent(parent.getId());
      // Set relative position to parent:
      entity.setPosRel(posAbs.subtract(parent.getPosAbs()));
      // Set relative velocity to parent:
      entity.setVelRel(velAbs.subtract(parent.getVelAbs()));

      double m = entity.getMass();
      double M = parent.getMass();
      init(entity, time);
      
      Planet p = entity.getPlanet();
      if(p != null) {
        // Sphere of Influence = semimajor axis * (m/M)^(2/5)
        p.r_soi = elements.a * Math.pow(m/M, 0.4);
      }
      
    } else {
      this.isDynamic = false;
      this.isParked = true;
      entity.setParent(-1);
      entity.setPosRel(posAbs);
      entity.setVelRel(velAbs);
      // Top parent: everything is within sphere of influence
      entity.getPlanet().r_soi = Double.POSITIVE_INFINITY;
    }
  }
  
  @Override
  public boolean isStatic() {
    return isParked;
  }

  @Override
  public PolyTrace getTrace() {
    if(!isParked) {
      return null;
    } else {
      return trace;
    }
  }
  
  @Override
  public void applyDeltaV(Entity e, double dv, double angle, long time) {
    if(isDynamic) {
      // Update Velocity:
      e.setVelRel(e.getVelRel().add(new Vector3d(dv, angle)));
      // Update Orbit:
      init(e, time);
      // Un-stick:
      isParked = false;
      trace = null;
    }
  }
  
  private Vector3d crossoverIntersect(Vector3d v1, Vector3d v2, Planet cb1, Planet cb2) {
    if(Double.isNaN(v2.x) ) {
      System.out.println("!!!ERROR: NaN new position at crossover intersect");
      return null;
    }
    // Find smaller radius: new or old?
    if(cb1.r_soi < cb2.r_soi) {
      double r = cb1.r_soi;
      // Calculate approximate intersection between v1-to-v2 and smaller radius circle:
      double r1 = v1.length();//origin.distanceTo(v1);
      double r2 = v2.length();//origin.distanceTo(v2);
      double x = (r - r1) / (r2 - r1); // ratio along vector v1tov2 at the xsection
      Vector3d v1tov2 = v2.subtractXY(v1);
      return v1.add(v1tov2.scale(x));
      //System.out.println("   v1 = " + v1);
      //System.out.println("   v2 = " + v2);
      //System.out.println("   r = " + r + "  r1 = " + r1 + "  r2 = " + r2);
      //System.out.println("   crossover ratio:" + x);
    } else {
      
      // TODO clipping when entering smaller rsoi
      return null;
    }
  }
  
  @Override
  public final void init(Entity entity, long time) {
    // Unstick:
    isParked = false;
    trace = null;
    
    // Configure Reference Point (initial point)
    Entity parent = entity.getParent();
    Planet planet = parent.getPlanet();
    refState = new StateVec(entity.getPosRel(), entity.getVelRel());
    refTime = time;
    orbitEndPoint = Vector3d.ZERO;

    // get orbital elements:
    elements = Kepler.getElements(refState, planet.mu);
    long tms = System.currentTimeMillis();

    path.reset();
    double a = elements.a;
    double e = elements.e0;
    int nsides = 500;
    
    double tmax = 3000; // TODO determine time to draw hyperbola from elements!
    double period;
    if(elements.isEllipse && elements.apogee < planet.r_soi) {
      period = elements.period;
    } else {
      period = tmax;
    }
    
    // Setup to draw Path:
    path.addPoint(new Vector3d(refState.r.x, refState.r.y, 0));
    Entity parentProj = parent; // projected parent
    int numSteps = 0;
    boolean crossover = false;
    double segLength = 1;
    Vector3d posR0, posR1; // relative positions from initial parent
    posR0 = refState.r;
    
    // Draw Path:
    double t = 0;
    while(t < period && !crossover) {
      // Increment time:
      t += period / nsides;
      if(t > period) t = period; // Cap at the Period (so that end == start)
      
      // find projected state vector of this object:
      StateVec projState = solve(parentProj, (long)(t + refTime));
      Vector3d pos = new Vector3d(projState.r.x, projState.r.y, 0);
      posR1 = projState.r;
      // if dynamic, project the whole universe to check intersections:
      if(isDynamic) {
        Main.universe().project((long)(t + refTime));
        // Use entity to get the absolute projected position:
        entity.setPosProjRel(projState.r);
        Vector3d posAbs = entity.getPosProjAbs();
        
        // check if we changed influence:
        Entity newParent = Main.universe().getProjectedInfluence(posAbs);
        if(parentProj != newParent) {
          // Crossed over into a new celestial body's influence (cbn):
          System.out.println("Orbit ends at influence of: "+newParent.getName());
          crossover = true;
          // Clip orbit:
          orbitEndPoint = crossoverIntersect(posR0, posR1,
            parentProj.getPlanet(), newParent.getPlanet());
          if(orbitEndPoint != null) {
            pos = orbitEndPoint;
          }
        }
      }
      path.addPoint(pos);
      segLength = Math.max(posR0.distanceTo(posR1), segLength);
      posR0 = posR1;
      numSteps++;
    }
    if(isDynamic) {
      path.setColor(Color.MAGENTA);
    } else {
      path.setColor(Color.CYAN);
    }
    path.load();
    
    viewOrbitCutoff = segLength/20000;
    if(viewOrbitCutoff > 300) {
      viewOrbitCutoff = 300;
    }
  }

  private StateVec solve(Entity parent, long t) {
    if(parent == null) {
      System.err.println("Null Parent");
    }
    if(parent.getPlanet() == null) {
      System.err.println("Null Planet:" + parent.toString());
    }
    StateVec state = Kepler.orbitPos(refState, t - refTime, parent.getPlanet().mu);
    if(Double.isNaN(state.r.x)) {
      System.out.println("BAD: solve(init = " + refState
        + "; t = " + (t - refTime) + "; Parent = "+parent.getName()
        + "; mu = " + parent.getPlanet().mu + ")");
    }
    return state;
  }
  
  @Override
  public void project(Entity e, long t) {
    // Assumed to never change parent when projecting!
    if(e.getParent() != null) {
      e.setPosProjRel(solve(e.getParent(), t).r);
    }
  }
  
  @Override
  public void update(Entity e, long time) {
    if(e.getParent() == null) {
      e.updatePosition();
    } else if(isParked) {
      e.setPosRel(trace.getPos());
      e.setVelRel(Vector3d.ZERO);
      e.setAngle(trace.getAngle());
    } else {
      // Previous state:
      Vector3d oldPos = e.getPosRel();
      Vector3d oldVel = e.getVelRel();
      // Calculate current state:
      StateVec state = solve(e.getParent(), time);
      e.setPosRel(state.r);
      e.setVelRel(state.v);
      
      if(isDynamic) {
        // Check if the influencing parent has changed:
        Entity parent = Main.universe().getInfluence(e.getPosAbs());
        if(e.getParent() == parent) { // no change, check collision:
          // Line segment from previous to current relative position:
          Vector3d newPos = e.getPosRel();
          Line3d line = new Line3d();
          line.setStartToEnd(oldPos, newPos);

          // Check Collisions:
          Polyline poly = e.getOutline();
          if(poly != null) {
            CollisionResult result = parent.getOutline().collide(line, poly, (float)e.getAngle());
            if(result != null) { // if collision occured:
              //System.out.println("COLLISION WITH: " + parent.getName());
              // remove from intersecting body:
              Vector3d pos = newPos.add(result.removalVector);
              e.setPosRel(pos);
              // Rebound:
              Vector3d vel = result.reboundDir.scale(oldVel.length());
              e.setVelRel(vel);
              if(vel.length() > 0.1) {
                // Re-calc orbit:
                init(parent, time);
              } else {
                isParked = true;
                trace = new PolyTrace(parent.getOutline());
                trace.setPos(result.segId, result.ratio);
                trace.setOffset(pos.subtract(trace.getTracePos()));
                trace.setAngleOffset(e.getAngle());
                //System.out.println(trace.getPos() + " == " + pos);
                //System.out.println(e.getAngle()*180/Math.PI + " == " + trace.getAngle()*180/Math.PI);
              }
              // TODO call appropriate Entity function to notify of collision
              // entity.impact(vel.length());  
            }
          }
        } else { // Orbiting a new parent:
          System.out.println("Now Orbiting: " + parent.getName());
          // Influencing body changed:
          e.setParent(parent.getId());
          // Set position and velocity relative to new cb:
          e.setPosRel(e.getPosAbs().subtract(parent.getPosAbs()));
          e.setVelRel(e.getVelAbs().subtract(parent.getVelAbs()));
          // Regenerate Orbit:
          init(e, time);
        }
      }
    }
  }

  @Override
  public void render(Entity e, Camera camera) {
    if(trace != null && Const.DRAW_OUTLINES) {
      vectorPl.reset();
      vectorPl.addPoint(trace.getTracePos());
      vectorPl.addPoint(e.getPosRel());
      vectorPl.setColor(Color.RED);
      vectorPl.load();
      vectorPl.render(camera, e.getParent().getPosAbs(), 0., Const.LAYER_OVERLAY);
    }
    if(e.getParent() != null && !isParked) {
      if(camera.getScale() > Const.VIEW_SCALE_ORBIT) { //viewOrbitCutoff) {
        if(path.getColor().a < 1f) {
          path.getColor().a += 0.08f;
        }
      } else {
        if(path.getColor().a > 0f) {
          path.getColor().a -= 0.08f;
        }
      }
      if(path.getColor().a > 0.1f) {
        path.render(camera, e.getParent().getPosAbs(), 0., Const.LAYER_UNDERLAY);
      }
    }
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(elements.isEllipse?"Ellipse":"Hyperbola");
    sb.append(String.format("  E = %1.03f", elements.e0));
    sb.append(String.format("  P = %1.03f", elements.period));
    return sb.toString();
  }
}
