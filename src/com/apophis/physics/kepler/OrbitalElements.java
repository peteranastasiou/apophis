package com.apophis.physics.kepler;

import com.apophis.maths.Vector3d;
import java.io.Serializable;

public class OrbitalElements implements Serializable {

  public Vector3d h; // angular momentum
  public double h0;  // angular momentum scalar
  public Vector3d e; // Eccentricity vector
  public double e0; // eccentricity
  public double ra; // right ascension of ascending node
  public double incl; // inclination
  public double w; // argument of perigee
  public double ta; // true anomaly
  public double a; // semimajor axis
  public double apogee; // furthest point
  public double perigee; // closest point
  public double period; // seconds. (negative one if hyperbolic)
  public boolean isEllipse;
}
